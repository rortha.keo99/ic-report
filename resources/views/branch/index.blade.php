@extends('admin.admin-master')
@section('content')
@section('title', 'Branch')
<section class="content ">
    <div class="row">
        <div class="col-xs-12">
            <section class="col-lg-9 connectedSortable">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">BRANCH</h3>
                        <button style="margin-bottom: 5px; margin-left: 15px" type="button" class="btn btn-success btn-xs" data-toggle="modal" data-target="#ModalCreate">New Branch</button>
                    </div>
                    @if($errors -> count() > 0)
                    <div class="alert error-message" style=" list-style: none;">
                        @foreach($errors->all() as $error)
                        <li>
                            {{ $error }}
                        </li>
                        @endforeach
                    </div>
                    @endif
                    <div class="box-body table-responsive">
                        @if (isset($branch))
                        <table class="ui celled table responsive nowrap table-sm table-hover" id="dataTable">
                            <thead>
                                <tr>
                                    <th>N<sup>O</sup></th>
                                    <th>BRANCH CODE</th>
                                    <th>BRANCH NAME</th>
                                    <th>DESCRIPTION</th>
                                    <th>DATE</th>
                                    <th>UPDATE</th>
                                    <th>ACTION</th>
                                    <th><input type="checkbox" id="check-all"></th>
                                </tr>
                            </thead>
                            @if (count($branch) > 0)
                            @foreach ($branch as $key => $br)
                            <tr>
                                <td>{{ ++$key }}</td>
                                <td>{{ $br->branch_code ?? 'NULL'}}</td>
                                <td>{{ $br->branch_name ?? 'NULL'}}</td>
                                <td>{{ $br->description ?? 'NULL'}}</td>
                                <td>{{ date('d.m.Y', strtotime($br->created_at)) }}</td>
                                <td>{{ date('d.m.Y', strtotime($br->updated_at)) }}</td>
                                <td>
                                    <button value="{{ $br->id }}" class="btn btn-success btn-xs rounded-0 editbtnBranch" data-toggle="tooltip" type="button" data-placement="top" title="Edit"><i class="fa fa-edit"></i></button>
                                    <div class="btn-group dropstart">
                                        <button class="btn btn-success btn-xs rounded-0 dropdown-toggle" type="button" data-toggle="dropdown">
                                            More option
                                        </button>
                                        <ul class="dropdown-menu">
                                            <li><a href="/branch/delete/{{ $br->id }}" class="delete-confirm" type="submit">Delete</a></li>
                                        </ul>
                                    </div>
                                </td>
                                <th><input type="checkbox" name="checkbox"></th>
                            </tr>
                            @endforeach
                            @endif
                        </table>
                        @endif
                    </div>
                </div>
            </section>
        </div>
    </div>
    @include('branch.create')
    @include('branch.edit')
</section>
@endsection