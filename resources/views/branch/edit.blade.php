<div class="modal fade" id="ModalEdit" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">UPDATE BRANCH</h4>
            </div>
            <form method="POST" id="branchdata" action="{{ url('branch/update') }}">
                <div class="modal-body">
                    {{ csrf_field() }}
                    {{ method_field('put') }}
                    <input type="hidden" name="_method" value="PUT">
                    <input type="hidden" name="branch_id" id="branch_id">
                    <div class="form-group row">
                        <div class="col-md-4">
                            <label for="inputState">Branch Code:</label>
                            <input type="text" class="form-control form-rounded" id="branch_code" name="branch_code" placeholder="Enter branch code..." autocomplete="off" required>
                        </div>
                        <div class="col-md-4">
                            <label for="inputState">Branch Name:</label>
                            <input type="text" class="form-control form-rounded" id="branch_name" name="branch_name" placeholder="Enter branch name..." autocomplete="off" required>
                        </div>
                        <div class="col-md-4">
                            <label for="inputZip">Description:</label>
                            <input type="text" class="form-control form-rounded" id="description" name="description" placeholder="Enter description..." autocomplete="off">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-success" id="submit">Update Data</button>
                </div>
            </form>
        </div>
    </div>
</div>
