@extends('admin.admin-master')
@section('content')
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <section class="col-lg-7 connectedSortable">
        <div class="nav-tabs-custom">
          <ul class="nav nav-tabs">
            <li class="active"><a href="#mywork" data-toggle="tab">My Work</a></li>
            <li><a href="#fileupload" data-toggle="tab">File upload</a></li>
          </ul>
          <div class="tab-content">
            <div class="active tab-pane" id="mywork">
              <div class="box box-primary">
                <div class="box-header">
                  <i class="ion ion-clipboard"></i>
                  <h3 class="box-title">To Do List</h3>
                </div>
                <div class="box-body">
                  <ul class="todo-list">
                    <li>
                      <span class="handle">
                        <i class="fa fa-ellipsis-v"></i>
                        <i class="fa fa-ellipsis-v"></i>
                      </span>
                      <span class="text">Service report</span>
                      <small class="label bg-label"></i> 2</small>
                    </li>
                    <li>
                      <span class="handle">
                        <i class="fa fa-ellipsis-v"></i>
                        <i class="fa fa-ellipsis-v"></i>
                      </span>
                      <span class="text">Pending repair</span>
                      <small class="label bg-label"></i> 4</small>
                    </li>
                    <li>
                      <span class="handle">
                        <i class="fa fa-ellipsis-v"></i>
                        <i class="fa fa-ellipsis-v"></i>
                      </span>
                      <span class="text">Keo Rortha</span>
                      <small class="label bg-label"></i> 29 IC</small>
                    </li>
                    <li>
                      <span class="handle">
                        <i class="fa fa-ellipsis-v"></i>
                        <i class="fa fa-ellipsis-v"></i>
                      </span>
                      <span class="text">Channa</span>
                      <small class="label bg-label"></i> 34 IC</small>
                    </li>
                    <li>
                      <span class="handle">
                        <i class="fa fa-ellipsis-v"></i>
                        <i class="fa fa-ellipsis-v"></i>
                      </span>
                      <span class="text">Pisey</span>
                      <small class="label bg-label"></i> 10 IC</small>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
            <!-- /.tab-pane -->
            <div class="tab-pane" id="fileupload">
              <!-- The timeline -->
              <div class="tab-content">
                <div class="active tab-pane" id="fileupload">
                  <!-- Post -->
                  <div class="box box-primary">
                    <div class="box-header">
                      <h3 class="box-title">FILE</h3>
                      <button style="margin-bottom: 5px; margin-left: 15px" type="button" class="btn btn-success btn-xs" data-toggle="modal" data-target="#ModalUpload">New File</button>
                    </div>
                    <div class="our-webcoderskull padding-lg">
                      <ul class="row">

                      </ul>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>
  </div>
</section>

@endsection