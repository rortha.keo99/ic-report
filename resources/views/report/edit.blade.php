<div class="modal fade" id="ModalEdit" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">UPDATE REPORT</h4>
            </div>
            <form method="POST" action="{{ url('/report/update') }}" enctype="multipart/form-data">
                <div class="modal-body">
                    {{ csrf_field() }}
                    {{ method_field('put') }}
                    <input type="hidden" name="_method" value="PUT">
                    <input type="hidden" name="report_id" id="report_id">
                    @if($errors -> count() > 0)
                    <div class="alert error-message" style=" list-style: none;">
                        @foreach($errors->all() as $error)
                        <li>
                            {{ $error }}
                        </li>
                        @endforeach
                    </div>
                    @endif
                    <div class="form-group row">
                        <div class="col-md-6">
                            <label for="inputState">Khmer name:</label>
                            <input type="text" class="form-control form-rounded" id="kh_name" name="kh_name" placeholder="Enter Khmer name..." required autocomplete="off">
                        </div>
                        <div class="col-md-6">
                            <label for="inputZip">English name:</label>
                            <input type="text" class="form-control form-rounded" id="eng_name" name="eng_name" placeholder="Enter English name..." autocomplete="off">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-12">
                            <label for="inputZip">Description:</label>
                            <textarea class="form-control form-rounded col-xs-12" id="description" name="description" rows="5" autocomplete="off" placeholder="Write your inspectation"></textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-success">Update Data</button>
                </div>
            </form>
        </div>
    </div>
</div>