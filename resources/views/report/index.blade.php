@extends('admin.admin-master')
@section('content')
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">
          <li class="active"><a href="#myreport" data-toggle="tab">My Report</a></li>
        </ul>
        <div class="tab-content">
          <div class="active tab-pane" id="myreport">
            <!-- Post -->
            <div class="box box-primary">
              <div class="box-header">
                <h3 class="box-title">REPORT</h3>
                <button style="margin-bottom: 5px; margin-left: 15px" type="button" class="btn btn-success btn-xs" data-toggle="modal" data-target="#ModalCreate">New Report</button>
              </div>
              @if($errors -> count() > 0)
                <div class="alert error-message" style=" list-style: none;">
                    @foreach($errors->all() as $error)
                    <li>
                        {{ $error }}
                    </li>
                    @endforeach
                </div>
                @endif
              <div class="box-body">
                <table class="ui celled table responsive nowrap table-sm table-hover" id="dataTable">
                  <thead class="table-dark">
                    <tr>
                      <th>N<sup>O</sup></th>
                      <th>KHMER</th>
                      <th>ENGLISH</th>
                      <th>FILE</th>
                      <th>DESCRIPTION</th>
                      <th>DATE</th>
                      <th>ACTION</th>
                      <th><input type="checkbox" id="check-all"></th>
                    </tr>
                  </thead>
                  @foreach($report as $key => $rep)
                  <tr>
                    <td>{{ ++ $key }}</td>
                    <td>{{ $rep -> kh_name }}</td>
                    <td>{{ $rep -> eng_name }}</td>
                    <td><a href="/uploads/report/{{ $rep->image }}" download="{{ $rep->image }}" class="download-confirm">{{ $rep -> image }}</a></td>
                    <td>{{ $rep -> description }}</td>
                    <td>{{ date('d-m-Y', strtotime($rep -> created_at)) }}</td>
                    <td>
                      <button value="{{ $rep -> id }}" class="btn btn-success btn-xs rounded-0 editbtnReport" data-toggle="tooltip" type="button" data-placement="top" title="Edit"><i class="fa fa-edit"></i></button>
                      <div class="btn-group dropstart">
                        <button class="btn btn-success btn-xs rounded-0 dropdown-toggle" type="button" data-toggle="dropdown">
                          More option
                        </button>
                        <ul class="dropdown-menu">
                          <li><a href="/uploads/report/{{ $rep->image }}" download="{{ $rep->image }}" class="download-confirm" type="submit">Download</a></li>
                          <li><a href="/report/delete/{{ $rep -> id }}" class="delete-confirm" type="submit">Delete</a></li>
                        </ul>
                      </div>
                    </td>
                    <th><input type="checkbox" name="checkbox"></th>
                  </tr>
                  @endforeach
                </table>
                
              </div>
            </div>
          </div>
          <!-- /.tab-pane -->
        </div>
      </div>
    </div>
  </div>
</section>
@include('report.create')
@include('report.edit')
@endsection