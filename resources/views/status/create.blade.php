<div class="modal fade" id="ModalCreate" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">NEW STATUS</h4>
            </div>
            <!-- <form method="POST" action="{{ url('/status/save') }}"> -->
            <div class="modal-body">
                <ul id="error_message"></ul>
                <ul id="success_message"></ul>
                <div class="form-group row">
                    <div class="col-md-6">
                        <label for="inputState">Status:</label>
                        <input type="text" class="form-control form-rounded status_name" placeholder="Enter status" required autocomplete="off">
                    </div>
                    <div class="col-md-6">
                        <label for="inputZip">Description:</label>
                        <input type="text" class="form-control form-rounded description" placeholder="Enter description" autocomplete="off">
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-success savebtnStatus">Save Data</button>
            </div>
            <!-- </form> -->
        </div>
    </div>
</div>