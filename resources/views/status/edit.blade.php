<div class="modal fade" id="ModalEdit" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">UPDATE STATUS</h4>
            </div>
            <form method="POST" id="statusid" action="{{ url('status/update') }}">
                <div class="modal-body">
                    {{ csrf_field() }}
                    {{ method_field('put') }}
                    <input type="hidden" name="_method" value="PUT">
                    <input type="hidden" name="status_id" id="status_id">
                    <div class="form-group row">
                        <div class="col-md-6">
                            <label for="inputState">Status:</label>
                            <input type="text" class="form-control form-rounded" id="status_name" name="status_name" placeholder="Enter status" autocomplete="off" required>
                        </div>
                        <div class="col-md-6">
                            <label for="inputZip">Description:</label>
                            <input type="text" class="form-control form-rounded" id="description" name="description" placeholder="Enter description" autocomplete="off">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-success" id="submit">Update Data</button>
                </div>
            </form>
        </div>
    </div>
</div>
