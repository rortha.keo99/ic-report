@extends('admin.admin-master')
@section('content')
<section class="content">
  <!-- Small boxes (Stat box) -->
  <div class="row">
    <div class="col-xs-12">
      <div class="box box-primary">
        <div class="box-header">
          <div class="col-lg-2">
            <!-- small box -->
            <div class="small-box bg-primary form-rounded">
              <div class="inner">
                <h3>{{ $count_user }}</h3>
                <p>Total Users</p>
              </div>
              <a href="{{ url('user') }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <div class="col-lg-2">
            <!-- small box -->
            <div class="small-box bg-primary form-rounded">
              <div class="inner">
                <h3>{{ $count_service }}</h3>
                <p>Service report</p>
              </div>
              <a href="{{ url('service') }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <div class="col-lg-2">
            <!-- small box -->
            <div class="small-box bg-primary form-rounded">
              <div class="inner">
                <h3>{{ $count_complete }}</h3>
                <p>Completed</p>
              </div>
              <a href="{{ url('service') }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-2">
            <!-- small box -->
            <div class="small-box bg-primary form-rounded">
              <div class="inner">
                <h3>{{ $count_sendtoservice }}</h3>
                <p>Send to Service</p>
              </div>
              <a href="{{ url('service') }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <div class="col-lg-2">
            <!-- small box -->
            <div class="small-box bg-primary form-rounded">
              <div class="inner">
                <h3>{{ $count_repairing }}</h3>
                <p>Repairing</p>
              </div>
              <a href="{{ url('service') }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <div class="col-lg-2">
            <!-- small box -->
            <div class="small-box bg-primary form-rounded">
              <div class="inner">
                <h3>{{ $count_retire }}</h3>
                <p>Retire</p>
              </div>
              <a href="{{ url('service') }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <div class="col-lg-2">
            <!-- small box -->
            <div class="small-box bg-primary form-rounded">
              <div class="inner">
                <h3>{{ $count_checking }}</h3>
                <p>Checking</p>
              </div>
              <a href="{{ url('service') }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <div class="col-lg-2">
            <!-- small box -->
            <div class="small-box bg-primary form-rounded">
              <div class="inner">
                <h3>{{ $count_product }}</h3>
                <p>Products</p>
              </div>
              <a href="{{ url('product') }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

@endsection
