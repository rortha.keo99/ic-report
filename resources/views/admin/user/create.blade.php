<div class="modal fade" id="ModalCreate" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">NEW USER</h4>
            </div>
            <form class="form-signin" method="POST" action="{{ route('register') }}">
                <div class="modal-body">
                    <div class="form-group row">
                        <div class="col-md-3">
                            @csrf
                            <label for="inputZip">Name:</label>
                            <input id="name" type="text"
                                class="form-control form-rounded @error('name') is-invalid @enderror" name="name"
                                value="{{ old('name') }}" required autocomplete="off" autofocus placeholder="Name">
                            @error('name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="col-md-3">
                            @csrf
                            <label for="inputZip">User name:</label>
                            <input id="username" type="text"
                                class="form-control form-rounded @error('username') is-invalid @enderror"
                                name="username" value="{{ old('username') }}" required autocomplete="off"
                                placeholder="User name or ID card">
                            @error('username')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="col-md-6">
                            @csrf
                            <label for="inputZip">Email:</label>
                            <input id="email" type="email"
                                class="form-control form-rounded @error('email') is-invalid @enderror" name="email"
                                value="{{ old('email') }}" required autocomplete="off" placeholder="Email">
                            @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-6">
                            @csrf
                            <label for="inputZip">Password:</label>
                            <input id="password" type="password"
                                class="form-control form-rounded @error('password') is-invalid @enderror"
                                name="password" required autocomplete="off" placeholder="Password">
                            @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="col-md-6">
                            @csrf
                            <label for="inputZip">Confirm password:</label>
                            <input id="password-confirm" type="password" class="form-control form-rounded"
                                name="password_confirmation" required autocomplete="off" placeholder="Confirm password">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-12">
                            <label for="inputZip">Role:</label>
                            <select class="form-control form-rounded" id="id" name="role" required>
                                <option selected disabled value="">-----SELECT-----</option>
                                @foreach (\App\Models\Roles::all() as $role)
                                    <option value="{{ $role->id }}">{{ $role->role }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <button class="btn btn-success btn-block btn-signin" type="submit">Register</button>
                </div>
            </form>
        </div>
    </div>
</div>
