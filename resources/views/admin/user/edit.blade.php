<div class="modal fade" id="ModalEdit" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">UPDATE USER</h4>
            </div>
            <form method="POST" action="{{ url('user/update') }}">
                <div class="modal-body">
                    {{ csrf_field() }}
                    {{ method_field('PUT') }}
                    <input type="hidden" id="user_id">
                    <div class="form-group row">
                        <div class="col-md-4">
                            <label for="inputState">Name:</label>
                            <input type="text" id="u_name" name="user_name" class="form-control form-rounded" placeholder="Name..." required autocomplete="off">
                        </div>
                        <div class="col-md-4">
                            <label for="inputZip">Email:</label>
                            <input id="u_email" type="user_email" class="form-control form-rounded" name="email" required autocomplete="off" placeholder="Email">
                        </div>
                        <div class="col-md-4">
                            <label for="inputState">Phone:</label>
                            <input id="u_phone" type="text" class="form-control form-rounded" name="user_phone" autocomplete="off" placeholder="Phone">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button class="btn btn-success" type="submit">Update</button>
                </div>
            </form>
        </div>
    </div>
</div>