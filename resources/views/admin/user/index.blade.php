@extends('admin.admin-master')
@section('content')
@section('title', 'User')
<section class="content">
    <div class="row">
        <div class="col-xs-12">
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="active"><a href="#user" data-toggle="tab">User</a></li>
              <li><a href="#role" data-toggle="tab">Roles</a></li>
            </ul>
            <div class="tab-content">
              <div class="active tab-pane" id="user">
                <!-- Post -->
                <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">USER</h3>
                    <button style="margin-bottom: 5px; margin-left: 15px" type="button" class="btn btn-success btn-xs" data-toggle="modal" data-target="#ModalCreate">New User</button>
                </div>
                <div class="box-body table-responsive">
                    @if(isset($user))
                    <table class="ui celled table responsive nowrap table-sm table-hover" id="dataTable">
                        <thead>
                            <tr>
                                <th>N<sup>O</sup></th>
                                <th>NAME</th>
                                <th>EMAIL</th>
                                <th>USER NAME</th>
                                <th>PHONE</th>
                                <th>DATE</th>
                                <th>UPDATE</th>
                                <th>ACTION</th>
                                <th><input type="checkbox" id="check-all"></th>
                            </tr>
                        </thead>
                        @if (count($user) > 0)
                        @foreach($user as $key => $us)
                        <tr>
                            <td>{{ ++$key }}</td>
                            <td>{{ $us -> name ?? 'NULL'}}</td>
                            <td>{{ $us -> email ?? 'NULL'}}</td>
                            <td>{{ $us -> username ?? 'NULL'}}</td>
                            <td>{{ $us -> phone ?? 'NULL'}}</td>
                            <td>{{ date('d.m.Y', strtotime($us -> created_at)) }}</td>
                            <td>{{ date('d.m.Y', strtotime($us -> updated_at)) }}</td>
                            <td>
                                <button value="{{ $us->id }}" class="btn btn-success btn-xs rounded-0 editbtnUser" data-toggle="tooltip" type="button" data-placement="top" title="Edit"><i class="fa fa-edit"></i></button>
                                <div class="btn-group dropstart">
                                    <button class="btn btn-success btn-xs rounded-0 dropdown-toggle" type="button" data-toggle="dropdown">
                                        Get option
                                    </button>
                                    <ul class="dropdown-menu">
                                        <li><a href="#" data-url="" class="status-editbtn">Edit</a></li>
                                        <li><a href="#" class="delete-confirm" type="submit">Delete</a></li>
                                    </ul>
                                </div>
                            </td>
                            <th><input type="checkbox" name="checkbox"></th>
                        </tr>
                        @endforeach
                        @endif
                    </table>
                    @endif
                </div>
            </div>
              </div>
              <!-- /.tab-pane -->
              <div class="tab-pane" id="role">
                <!-- The timeline -->
                <div class="tab-content">
              <div class="active tab-pane" id="user">
                <!-- Post -->
                <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">USER</h3>
                    <button style="margin-bottom: 5px; margin-left: 15px" type="button" class="btn btn-success btn-xs" data-toggle="modal" data-target="#ModalCreate">New User</button>
                </div>
                <div class="box-body table-responsive">
                    
                    <table class="ui celled table responsive nowrap table-sm table-hover" id="dataTable">
                        <thead>
                            <tr>
                                <th>N<sup>O</sup></th>
                                <th>ROLE NAME</th>
                                <th>DATE</th>
                                <th>UPDATE</th>
                                <th>ACTION</th>
                                <th><input type="checkbox" id="check-all"></th>
                            </tr>
                        </thead>
                        <tr>
                            <td>dsffs</td>
                            <td>sfsfs</td>
                            <td>sdfsfs</td>
                            <td>sfsfs</td>
                            <td>
                                <button value="{{ $us->id }}" class="btn btn-success btn-xs rounded-0 editbtnUser" data-toggle="tooltip" type="button" data-placement="top" title="Edit"><i class="fa fa-edit"></i></button>
                                <div class="btn-group dropstart">
                                    <button class="btn btn-success btn-xs rounded-0 dropdown-toggle" type="button" data-toggle="dropdown">
                                        Get option
                                    </button>
                                    <ul class="dropdown-menu">
                                        <li><a href="#" data-url="" class="status-editbtn">Edit</a></li>
                                        <li><a href="#" class="delete-confirm" type="submit">Delete</a></li>
                                    </ul>
                                </div>
                            </td>
                            <th><input type="checkbox" name="checkbox"></th>
                        </tr>
                       
                    </table>
                </div>
            </div>
              </div>
            </div>
          </div>
        </div>
    </div>
    @include('admin.user.create')
    @include('admin.user.edit')
</section>
</section>
</section>
@endsection