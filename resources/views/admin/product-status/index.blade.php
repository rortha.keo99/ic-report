@extends('admin.admin-master')
@section('content')
@section('title', 'Product Status')
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <section class="col-lg-9 connectedSortable">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">PRODUCT STATUS</h3>
                        <button style="margin-bottom: 5px; margin-left: 15px" type="button" class="btn btn-success btn-xs" data-toggle="modal" data-target="#ModalCreate">New Product status</button>
                    </div>
                    @if($errors -> count() > 0)
                    <div class="alert error-message" style=" list-style: none;">
                        @foreach($errors->all() as $error)
                        <li>
                            {{ $error }}
                        </li>
                        @endforeach
                    </div>
                    @endif
                    <div class="box-body table-responsive">
                        @if (isset($prostatus))
                        <table class="ui celled table responsive nowrap table-sm table-hover" id="dataTable">
                            <thead class="table-dark">
                                <tr>
                                    <th>N<sup>O</sup></th>
                                    <th>NAME</th>
                                    <th>DESCRIPTION</th>
                                    <th>DATE</th>
                                    <th>UPDATE AT</th>
                                    <th>ACTION</th>
                                    <th><input type="checkbox" id="check-all"></th>
                                </tr>
                            </thead>
                            @if (count($prostatus) > 0)
                            @foreach ($prostatus as $key => $prostat)
                            <tr>
                                <td>{{ ++$key }}</td>
                                <td>{{ $prostat->pro_status }}</td>
                                <td>{{ $prostat->description }}</td>
                                <td>{{ date('d.m.Y', strtotime($prostat->created_at)) }}</td>
                                <td>{{ date('d.m.Y', strtotime($prostat->updated_at)) }}</td>
                                <td>
                                    <button value="{{ $prostat->id }}" class="btn btn-success btn-xs rounded-0 editbtnProStatus" data-toggle="tooltip" type="button" data-placement="top" title="Edit"><i class="fa fa-edit"></i></button>
                                    <div class="btn-group dropstart">
                                        <button class="btn btn-success btn-xs rounded-0 dropdown-toggle" type="button" data-toggle="dropdown">
                                            More option
                                        </button>
                                        <ul class="dropdown-menu">
                                            <li><a href="/prostatus/delete/{{ $prostat->id }}" class="delete-confirm" type="submit">Delete</a></li>
                                        </ul>
                                    </div>
                                </td>
                                <th><input type="checkbox" name="checkbox"></th>
                            </tr>
                            @endforeach
                            @endif
                        </table>
                        @endif
                    </div>
                </div>
            </section>
        </div>
    </div>
    @include('admin.product-status.create')
    @include('admin.product-status.edit')
</section>
@endsection