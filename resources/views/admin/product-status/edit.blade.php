<div class="modal fade" id="ModalEdit" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">UPDATE PRODUCT STATUS</h4>
            </div>
            <form method="POST" action="{{ url('/prostatus/update') }}">
                {{ csrf_field() }}
                {{ method_field('PUT') }}
                <input type="hidden" name="_method" value="PUT">
                <input type="hidden" name="prostatus_id" id="prostatus_id">
                <div class="modal-body">
                    <div class="form-group row">
                        <div class="col-md-6">
                            <label for="inputState">Product status:</label>
                            <input type="text" class="form-control form-rounded" id="pro_status" name="pro_status" placeholder="Enter product status..." required autocomplete="off">
                        </div>
                        <div class="col-md-6">
                            <label for="inputZip">Description:</label>
                            <input type="text" class="form-control form-rounded" id="description" name="description" placeholder="Enter description..." autocomplete="off">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-success">Save Data</button>
                </div>
            </form>
        </div>
    </div>
</div>