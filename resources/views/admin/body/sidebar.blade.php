<aside class="main-sidebar">
    <section class="sidebar">
        <!-- <div class="user-panel">
                    <div class="pull-left image">
                        <img src="dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
                    </div>
                    <div class="pull-left info">
                        <p>Rortha</p>
                        <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                    </div>
                </div> -->
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">MAIN NAVIGATION</li>
            <li>
                <a href="{{ route('dashboard') }}">
                    <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                </a>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-edit"></i> <span>Information</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="/service"><i class="fa fa-circle-o"></i> Service check</a></li>
                    <li><a href="/branch"><i class="fa fa-circle-o"></i> Branch</a></li>
                    <li><a href="/category"><i class="fa fa-circle-o"></i> Category</a></li>
                    <li><a href="/status"><i class="fa fa-circle-o"></i> Status</a></li>
                    <li><a href="/condition"><i class="fa fa-circle-o"></i>Condition</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-table"></i> <span>Stock management</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="/product"><i class="fa fa-circle-o"></i>Product</a></li>
                    <li><a href="/order"><i class="fa fa-circle-o"></i> Order</a></li>
                    <li><a href="/protype"><i class="fa fa-circle-o"></i>Product Type</a></li>
                    <li><a href="/prostatus"><i class="fa fa-circle-o"></i>Status</a></li>
                    <li><a href="/unit"><i class="fa fa-circle-o"></i> Unit</a></li>
                </ul>
            </li>
            <li>
                <a href="{{ route('report') }}">
                    <i class="fa fa-file-text"></i> <span>Report</span>
                </a>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-user"></i> <span>Management</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="/user"><i class="fa fa-circle-o"></i> Users</a></li>
                </ul>
            </li>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>
