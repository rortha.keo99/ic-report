@extends('admin.admin-master')
@section('content')
@section('title', 'Product')
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">PRODUCT</h3>
                    <button style="margin-bottom: 5px; margin-left: 15px" type="button" class="btn btn-success btn-xs" data-toggle="modal" data-target="#ModalCreate">New Product</button>
                </div>
                @if($errors -> count() > 0)
                <div class="alert error-message" style=" list-style: none;">
                    @foreach($errors->all() as $error)
                    <li>
                        {{ $error }}
                    </li>
                    @endforeach
                </div>
                @endif
                <div class="box-body table-responsive">
                    @if(isset($product))
                    <table class="ui celled table responsive nowrap table-sm table-hover" id="dataTable">
                        <thead>
                            <tr>
                                <th>N<sup>O</sup></th>
                                <th>CODE ITEM</th>
                                <th>BARCODE</th>
                                <th>NAME</th>
                                <th>TYPE</th>
                                <th>QUANTITY</th>
                                <th>PRICE</th>
                                <th>STATUS</th>
                                <th>DESCRIPTION</th>
                                <th>RESPONDER</th>
                                <th>CREATE DATE</th>
                                <th>UPDATE AT</th>
                                <th>ACTION</th>
                                <th><input type="checkbox" id="check-all"></th>
                            </tr>
                        </thead>
                        @if(count($product) > 0)
                        @foreach($product as $key => $pro)
                        <tr>
                            <td>{{ ++$key }}</td>
                            <td>{{ $pro-> code_item	}}</td>
                            <td>{{ $pro-> barcode}}</td>
                            <td>{{ $pro-> product_name }}</td>
                            <td>{{ $pro-> ProductType -> type_name ?? 'NULL' }}</td>
                            <td>{{ $pro-> quantity }}-{{ $pro-> Unit -> unit_name ?? 'NULL' }}</td>
                            <td>{{ $pro-> product_price	 }}-{{ $pro-> Currency -> currency ?? 'NULL' }}</td>
                            <td>{{ $pro-> Status -> pro_status ?? 'NULL' }}</td>
                            <td>{{ $pro-> description }}</td>
                            <td>{{ $pro-> User -> name ?? 'NULL' }}</td>
                            <td>{{ date('d-m-Y', strtotime($pro-> created_at)) }}</td>
                            <td>{{ date('d-m-Y', strtotime($pro-> updated_at)) }}</td>
                            <td>
                                <button value="{{ $pro -> id }}" class="btn btn-success btn-xs rounded-0 editbtnProduct" data-toggle="tooltip" type="button" data-placement="top" title="Edit"><i class="fa fa-edit"></i></button>
                                <div class="btn-group dropstart">
                                    <button class="btn btn-success btn-xs rounded-0 dropdown-toggle" type="button" data-toggle="dropdown">
                                        More option
                                    </button>
                                    <ul class="dropdown-menu">
                                        <li><a href="/product/delete/{{ $pro->id }}" class="delete-confirm" type="submit">Delete</a></li>
                                    </ul>
                                </div>
                            </td>
                            <th><input type="checkbox" name="checkbox"></th>
                        </tr>
                        @endforeach
                        @endif
                    </table>
                    @endif
                </div>
            </div>
        </div>
    </div>
    @include('admin.product.create')
    @include('admin.product.edit')
</section>
@endsection