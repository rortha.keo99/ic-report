<div class="modal fade" id="ModalCreate" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">NEW PRODUCT</h4>
            </div>
            <form method="POST" action="{{ url('/product/save') }}" enctype="multipart/form-data">
                <div class="modal-body">
                    {{ csrf_field() }}
                    {{ method_field('POST')}}
                    <div class="form-group row">
                        <div class="col-md-4">
                            <label for="inputState">Barcode:</label>
                            <input type="text" class="form-control form-rounded" name="barcode" placeholder="Enter barcode..." autocomplete="off">
                        </div>
                        <div class="col-md-4">
                            <label for="inputState">Product name:</label>
                            <input type="text" class="form-control form-rounded" name="product_name" placeholder="Enter product name..." required autocomplete="off">
                        </div>
                        <div class="col-md-4">
                            <label for="inputZip">Type:</label>
                            <select class="form-control form-rounded" id="id" name="product_type" required>
                                <option selected disabled value="">-----SELECT-----</option>
                                @foreach (\App\Models\ProductType::all() as $protype)
                                <option value="{{ $protype->id }}">{{ $protype->type_name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-4">
                            <label for="inputState">Quantity:</label>
                            <input type="number" class="form-control form-rounded" name="quantity" placeholder="Enter quantity..." required autocomplete="off">
                        </div>
                        <div class="col-md-4">
                            <label for="inputState">Unit:</label>
                            <select class="form-control form-rounded" id="id" name="unit_id" required>
                                <option selected disabled value="">-----SELECT-----</option>
                                @foreach (\App\Models\Unit::all() as $unit)
                                <option value="{{ $unit->id }}">{{ $unit->unit_name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-2">
                            <label for="inputZip">Price:</label>
                            <input type="number" class="form-control form-rounded" name="product_price" placeholder="Per item..." autocomplete="off">
                        </div>
                        <div class="col-md-2">
                            <label for="inputState">Currency:</label>
                            <select class="form-control form-rounded" id="id" name="currency_id">
                                <option selected disabled value="">-----SELECT-----</option>
                                @foreach (\App\Models\Currency::all() as $cur)
                                <option value="{{ $cur->id }}">{{ $cur->currency }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-6">
                            <label for="inputState">Status:</label>
                            <select class="form-control form-rounded" id="id" name="product_status_id" required>
                                <option selected disabled value="">-----SELECT-----</option>
                                @foreach (\App\Models\ProductStatus::all() as $pst)
                                <option value="{{ $pst->id }}">{{ $pst->pro_status ?? 'NULL'}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-6">
                            <label for="inputZip">Responder:</label>
                            <select class="form-control form-rounded" id="id" name="responder_id" required>
                                <option selected disabled value="">-----SELECT-----</option>
                                @foreach (\App\Models\User::all() as $us)
                                <option value="{{ $us->id }}">{{ $us->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-12">
                            <label for="inputState">Description:</label>
                            <textarea class="form-control form-rounded col-xs-12" rows="5" name="description" autocomplete="off" placeholder="Write your remark"></textarea>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-12">
                            <label for="formFileMultiple" class="form-label">Upload File:</label>
                            <input class="form-control form-rounded" type="file" name="image" multiple />
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-success">Save Data</button>
                </div>
            </form>
        </div>
    </div>
</div>