<div class="modal fade" id="ModalEdit" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">UPDATE PRODUCT TYPE</h4>
            </div>
            <form method="POST" action="{{ url('protype/update') }}">
                <div class="modal-body">
                    {{ csrf_field() }}
                    {{ method_field('put') }}
                    <input type="hidden" name="_method" value="PUT">
                    <input type="hidden" name="protype_id" id="protype_id">
                    <div class="form-group row">
                        <div class="col-md-6">
                            <label for="inputState">Product Type:</label>
                            <input type="text" class="form-control form-rounded" id="typename" name="type_name" placeholder="Enter product type..." required autocomplete="off">
                        </div>
                        <div class="col-md-6">
                            <label for="inputZip">Description:</label>
                            <input type="text" class="form-control form-rounded" id="description" name="description" placeholder="Enter your remark..." autocomplete="off">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-success">Update Data</button>
                </div>
            </form>
        </div>
    </div>
</div>