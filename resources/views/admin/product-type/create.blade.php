<div class="modal fade" id="ModalCreate" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">NEW PRODUCT TYPE</h4>
            </div>
            <form method="POST" action="{{ url('/protype/save') }}" enctype="multipart/form-data">
                <div class="modal-body">
                    {{ csrf_field() }}
                    {{ method_field('POST') }}
                    <div class="form-group row">
                        <div class="col-md-6">
                            <label for="inputState">Product Type:</label>
                            <input type="text" class="form-control form-rounded" name="type_name" placeholder="Enter product type..." required autocomplete="off">
                        </div>
                        <div class="col-md-6">
                            <label for="formFileMultiple" class="form-label">Upload File:</label>
                            <input class="form-control form-rounded" type="file" name="image" multiple />
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-12">
                            <label for="inputZip">Description:</label>
                            <textarea class="form-control form-rounded col-xs-12" rows="5" name="description" autocomplete="off" placeholder="Write your remark"></textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-success">Save Data</button>
                </div>
            </form>
        </div>
    </div>
</div>