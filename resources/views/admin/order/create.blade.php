<div class="modal fade" id="ModalCreate" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">NEW ORDER</h4>
            </div>
            <!-- @error('order_name')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror -->
            <!-- <form method="POST" action="{{ url('order/save') }}" enctype="multipart/form-data" class="was-validated"> -->
            <div class="modal-body">
                <ul id="error_message"></ul>
                <ul id="success_message"></ul>
                <div class="form-group row">
                    <div class="col-md-4">
                        <label for="inputState">Order name:</label>
                        <input type="text" class="form-control form-rounded order_name" placeholder="Enter customer..." autocomplete="off" required>
                    </div>
                    <div class="col-md-4">
                        <label for="inputState">Phone:</label>
                        <input type="text" class="form-control form-rounded phone" placeholder="Enter phone..." autocomplete="off">
                    </div>
                    <div class="col-md-4">
                        <label for="inputZip">Branch:</label>
                        <select class="form-control form-rounded branch_id" required>
                            <option selected disabled value="">-----SELECT-----</option>
                            @foreach (\App\Models\Branch::all() as $br)
                            <option value="{{ $br->id }}">
                                {{ $br->branch_code ?? 'NULL' }}-{{ $br->branch_name ?? 'NULL'}}
                            </option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-6">
                        <label for="inputState">Product name:</label>
                        <select class="form-control form-rounded product_name_id" required>
                            <option selected disabled value="">-----SELECT-----</option>
                            @foreach (\App\Models\Product::all() as $pro)
                            <option value="{{ $pro->id }}">
                                {{ $pro -> product_name ?? 'NULL'}}
                            </option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-md-3">
                        <label for="inputZip">Quantity:</label>
                        <input type="number" class="form-control form-rounded order_quantity" placeholder="Enter item quantity..." autocomplete="off">
                    </div>
                    <div class="col-md-3">
                        <label for="inputZip">Responder:</label>
                        <select class="form-control form-rounded responder_id" required>
                            <option selected disabled value="">-----SELECT-----</option>
                            @foreach (\App\Models\User::all() as $us)
                            <option value="{{ $us->id }}">
                                {{ $us->name ?? 'NULL' }}
                            </option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-12">
                        <label for="inputState">Description:</label>
                        <textarea class="form-control form-rounded col-xs-12 description" rows="5" autocomplete="off" placeholder="Write your remark"></textarea>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-success savebtnOrder">Save Data</button>
            </div>
            <!-- </form> -->
        </div>
    </div>
</div>