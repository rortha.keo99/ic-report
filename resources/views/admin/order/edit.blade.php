<div class="modal fade" id="ModalEdit" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">UPDATE ORDER</h4>
            </div>
            <form method="POST" action="{{ url('/order/update') }}" enctype="multipart/form-data">
                {{ csrf_field() }}
                {{ method_field('PUT')}}
                <input type="hidden" name="_method" value="PUT">
                <input type="hidden" name="order_id" id="order_id">
                <div class="modal-body">
                    <div class="form-group row">
                        <div class="col-md-4">
                            <label for="inputState">Order name:</label>
                            <input type="text" class="form-control form-rounded" id="order_name" name="order_name" placeholder="Enter customer..." required autocomplete="off">
                        </div>
                        <div class="col-md-4">
                            <label for="inputState">Phone:</label>
                            <input type="text" class="form-control form-rounded" id="phone" name="phone" placeholder="Enter phone..." required autocomplete="off">
                        </div>
                        <div class="col-md-4">
                            <label for="inputZip">Branch:</label>
                            <select class="form-control form-rounded" id="branch_id" name="branch_id">
                                <option selected disabled value="">-----SELECT-----</option>
                                @foreach (\App\Models\Branch::all() as $br)
                                <option value="{{ $br->id }}">
                                    {{ $br->branch_code }}-{{ $br->branch_name }}
                                </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-6">
                            <label for="inputState">Product name:</label>
                            <select class="form-control form-rounded" id="product_name_id" name="product_name_id">
                                <option selected disabled value="">-----SELECT-----</option>
                                @foreach (\App\Models\Product::all() as $pro)
                                <option value="{{ $pro->id }}">
                                    {{ $pro -> product_name ?? 'NULL'}}
                                </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-3">
                            <label for="inputZip">Quantity:</label>
                            <input type="number" class="form-control form-rounded" id="order_quantity" name="order_quantity" placeholder="Enter price per item..." autocomplete="off">
                        </div>
                        <div class="col-md-3">
                        <label for="inputZip">Responder:</label>
                        <select class="form-control form-rounded" id="responder_id" name="responder_id" required>
                            <option selected disabled value="">-----SELECT-----</option>
                            @foreach (\App\Models\User::all() as $us)
                            <option value="{{ $us->id }}">
                                {{ $us->name ?? 'NULL' }}
                            </option>
                            @endforeach
                        </select>
                    </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-12">
                            <label for="inputState">Description:</label>
                            <textarea class="form-control form-rounded col-xs-12" rows="5" id="description" name="description" required autocomplete="off" placeholder="Write your remark"></textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-success">Update Data</button>
                </div>
            </form>
        </div>
    </div>
</div>