@extends('admin.admin-master')
@section('content')
@section('title', 'Order')
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">ORDER</h3>
                    <button style="margin-bottom: 5px; margin-left: 15px" type="button" class="btn btn-success btn-xs" data-toggle="modal" data-target="#ModalCreate">New Order</button>
                </div>
                @if($errors -> count() > 0)
                <div class="alert error-message" style=" list-style: none;">
                    @foreach($errors->all() as $error)
                    <li>
                        {{ $error }}
                    </li>
                    @endforeach
                </div>
                @endif
                <div class="box-body table-responsive">
                    @if(isset($order))
                    <table class="ui celled table responsive nowrap table-sm table-hover" id="dataTable">
                        <thead class="table-dark">
                            <tr>
                                <th>N<sup>O</sup></th>
                                <th>ORDER CODE</th>
                                <th>ODER</th>
                                <th>PHONE</th>
                                <th>PRODUCT</th>
                                <th>PRICE</th>
                                <th>AMOUNT</th>
                                <th>TOTAL</th>
                                <th>BRANCH</th>
                                <th>RESPONDER</th>
                                <th>DESCRIPTION</th>
                                <th>DATE</th>
                                <th>UPDATE AT</th>
                                <th>ACTION</th>
                                <th><input type="checkbox" id="check-all"></th>
                            </tr>
                        </thead>
                        @if(count($order) > 0)
                        @foreach($order as $key => $ord)
                        <?php
                        $total = 0;
                        $total += ($ord['order_quantity'] * $ord->Product->product_price);
                        ?>
                        <tr>
                            <td>{{ ++$key }}</td>
                            <td>{{ $ord -> code_order }}</td>
                            <td>{{ $ord -> order_name }}</td>
                            <td>{{ $ord -> phone }}</td>
                            <td>{{ $ord -> Product -> product_name }}</td>
                            <td>{{ $ord -> Product -> product_price }}-{{ $ord -> Product -> Currency -> currency }}</td>
                            <td>{{ $ord -> order_quantity }}-{{ $ord -> Product -> Unit -> unit_name }}</td>
                            <td>{{ $total }}-{{ $ord -> Product -> Currency -> currency }}</td>
                            <td>{{ $ord -> Branch -> branch_name }}</td>
                            <td>{{ $ord -> Responder -> name }}</td>
                            <td>{{ $ord -> description }}</td>
                            <td>{{ date('d-m-Y', strtotime( $ord -> created_at ))}}</td>
                            <td>{{ date('d-m-Y', strtotime( $ord -> updated_at ))}}</td>
                            <td>
                                <button value="{{ $ord -> id }}" class="btn btn-success btn-xs rounded-0 editbtnOrder" data-toggle="tooltip" type="button" data-placement="top" title="Edit"><i class="fa fa-edit"></i></button>
                                <div class="btn-group dropstart">
                                    <button class="btn btn-success btn-xs rounded-0 dropdown-toggle" type="button" data-toggle="dropdown">
                                        More option
                                    </button>
                                    <ul class="dropdown-menu">
                                        <li><a href="/order/delete/{{ $ord->id }}" class="delete-confirm" type="submit">Delete</a></li>
                                    </ul>
                                </div>
                            </td>
                            <th><input type="checkbox" name="checkbox"></th>
                        </tr>
                        @endforeach
                        @endif
                    </table>
                    @endif
                </div>
            </div>
        </div>
    </div>
    @include('admin.order.create')
    @include('admin.order.edit')
</section>
@endsection