@extends('admin.admin-master')
@section('content')
@section('title', 'Unit')
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <section class="col-lg-9 connectedSortable">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">UNIT</h3>
                        <button style="margin-bottom: 5px; margin-left: 15px" type="button" class="btn btn-success btn-xs" data-toggle="modal" data-target="#ModalCreate">New Unit</button>
                    </div>
                    @if($errors -> count() > 0)
                    <div class="alert error-message" style=" list-style: none;">
                        @foreach($errors->all() as $error)
                        <li>
                            {{ $error }}
                        </li>
                        @endforeach
                    </div>
                    @endif
                    <div class="box-body table-responsive">
                        @if(isset($unit))
                        <table class="ui celled table responsive nowrap table-sm table-hover" id="dataTable">
                            <thead class="table-dark">
                                <tr>
                                    <th>N<sup>O</sup></th>
                                    <th>NAME</th>
                                    <th>DESCRIPTION</th>
                                    <th>DATE</th>
                                    <th>UPDATE AT</th>
                                    <th>ACTION</th>
                                    <th><input type="checkbox" id="check-all"></th>
                                </tr>
                            </thead>
                            @if(count($unit) > 0)
                            @foreach($unit as $key => $un)
                            <tr>
                                <td>{{ ++$key }}</td>
                                <td>{{ $un-> unit_name }}</td>
                                <td>{{ $un-> description }}</td>
                                <td>{{ date('d-m-Y', strtotime($un-> created_at)) }}</td>
                                <td>{{ date('d-m-Y', strtotime($un-> updated_at)) }}</td>
                                <td>
                                    <button value="{{ $un -> id }}" class="btn btn-success btn-xs rounded-0 editbtnUnit" data-toggle="tooltip" type="button" data-placement="top" title="Edit"><i class="fa fa-edit"></i></button>
                                    <div class="btn-group dropstart">
                                        <button class="btn btn-success btn-xs rounded-0 dropdown-toggle" type="button" data-toggle="dropdown">
                                            More option
                                        </button>
                                        <ul class="dropdown-menu">
                                            <li><a href="/unit/delete/{{ $un->id }}" class="delete-confirm" type="submit">Delete</a></li>
                                        </ul>
                                    </div>
                                </td>
                                <th><input type="checkbox" name="checkbox"></th>
                            </tr>
                            @endforeach
                            @endif
                        </table>
                        @endif
                    </div>
                </div>
            </section>
        </div>
    </div>
    @include('admin.unit.create')
    @include('admin.unit.edit')
</section>
@endsection