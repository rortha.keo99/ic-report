<div class="modal fade" id="ModalEdit" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">UPDATE UNIT</h4>
            </div>
            <form method="POST" action="{{ url('unit/update') }}">
                {{ csrf_field() }}
                {{ method_field('PUT') }}
                <input type="hidden" name="_method" value="PUT">
                <input type="hidden" name="unit_id" id="unit_id">
                <div class="modal-body">
                    <div class="form-group row">
                        <div class="col-md-6">
                            <label for="inputState">Unit name:</label>
                            <input type="text" class="form-control form-rounded" id="unit_name" name="unit_name" placeholder="Enter unit name..." required autocomplete="off">
                        </div>
                        <div class="col-md-6">
                            <label for="inputZip">Description:</label>
                            <input class="form-control form-rounded col-xs-12" rows="5" id="description" name="description" required autocomplete="off" placeholder="Write your remark"></input>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-success">Update Data</button>
                </div>
            </form>
        </div>
    </div>
</div>