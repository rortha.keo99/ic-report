<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@yield('title')</title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="stylesheet" href="{{ asset('bower_components/bootstrap/dist/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('bower_components/font-awesome/css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('bower_components/Ionicons/css/ionicons.min.css') }}">
    <link rel="stylesheet" href="{{ asset('dist/css/AdminLTE.min.css') }}">
    <link rel="stylesheet" href="{{ asset('dist/css/skins/_all-skins.min.css') }}">
    <link rel="stylesheet" href="{{ asset('bower_components/morris.js/morris.css') }}">
    <link rel="stylesheet" href="{{ asset('bower_components/jvectormap/jquery-jvectormap.css') }}">
    <link rel="stylesheet" href="{{ asset('bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('bower_components/bootstrap-daterangepicker/daterangepicker.css') }}">
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') }}">
    <link rel="stylesheet" href="{{ asset('https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic') }}">
    <link rel="stylesheet" href="{{ asset('https://cdn.datatables.net/1.12.1/css/dataTables.bootstrap5.min.css') }}">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css">
</head>
<!-- sidebar-collapse -->

<body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">
        @include('admin.body.header')
        @include('admin.body.sidebar')

        <div class="content-wrapper">
            @yield('content')
        </div>
        <!-- /.content-wrapper -->
        @include('admin.body.footer')
    </div>


    <script src="bower_components/jquery/dist/jquery.min.js"></script>
    <script src="bower_components/jquery-ui/jquery-ui.min.js"></script>
    <script>
        $.widget.bridge('uibutton', $.ui.button);
    </script>
    <script src=" {{ asset('bower_components/bootstrap/dist/js/bootstrap.min.js') }}"></script>
    <script src=" {{ asset('bower_components/raphael/raphael.min.js') }}"></script>
    <script src=" {{ asset('bower_components/morris.js/morris.min.js') }}"></script>
    <script src=" {{ asset('bower_components/jquery-sparkline/dist/jquery.sparkline.min.js') }}"></script>
    <script src=" {{ asset('plugins/jvectormap/jquery-jvectormap-1.2.2.min.js') }}"></script>
    <script src=" {{ asset('plugins/jvectormap/jquery-jvectormap-world-mill-en.js') }}"></script>
    <script src=" {{ asset('bower_components/jquery-knob/dist/jquery.knob.min.js') }}"></script>
    <script src=" {{ asset('bower_components/moment/min/moment.min.js') }}"></script>
    <script src=" {{ asset('bower_components/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
    <script src=" {{ asset('bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
    <script src=" {{ asset('plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') }}"></script>
    <script src=" {{ asset('bower_components/jquery-slimscroll/jquery.slimscroll.min.js') }}"></script>
    <script src=" {{ asset('bower_components/fastclick/lib/fastclick.js') }}"></script>
    <script src=" {{ asset('dist/js/adminlte.min.js') }}"></script>
    <!-- <script src=" {{ asset('dist/js/pages/dashboard.js') }}"></script> -->
    <script src=" {{ asset('dist/js/demo.js') }}"></script>
    <script src=" {{ asset('js/app.js') }}"></script>
    <script src=" {{ asset('js/script.js') }}"></script>
    <script src=" {{ asset('https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js') }}"></script>
    <script src=" {{ asset('https://cdn.datatables.net/1.10.25/js/jquery.dataTables.min.js') }}"></script>
    <script src=" {{ asset('https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js') }}"></script>
    <script src=" {{ asset('https://cdn.datatables.net/1.12.1/js/dataTables.bootstrap5.min.js') }}"></script>

    @if (Session::has('save'))
    <script>
        swal("Congratulation!", "Your data has been saved", "success");
    </script>
    @endif

    @if (Session::has('update'))
    <script>
        swal("Congratulation!", "Your data has been updated", "success");
    </script>
    @endif

    @if (Session::has('delete'))
    <script>
        swal("Congratulation!", "Your data has been deleted", "success");
    </script>
    @endif

    @if (Session::has('message'))
    <script>
        swal("Congratulation!", "Your data has been deleted", "primary");
    </script>
    @endif
</body>

</html>
