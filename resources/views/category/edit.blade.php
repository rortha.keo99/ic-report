<div class="modal fade" id="ModalEdit" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">UPDATE CATEGORY</h4>
            </div>
            <form method="POST" id="categorydata" action="{{ url('/category/update') }}">
                    @csrf
                    @method('PUT')
                    <input type="hidden" name="_method" value="PUT">
                    <input type="hidden" name="cate_id" id="cate_id">
                <div class="modal-body">
                    <div class="form-group row">
                        <div class="col-md-6">
                            <label for="inputState">Category:</label>
                            <input type="text" name="category_name" class="form-control form-rounded" id="category_name" placeholder="Enter category name..." autocomplete="off" required>
                        </div>
                        <div class="col-md-6">
                            <label for="inputZip">Description:</label>
                            <input type="text" name="description" class="form-control form-rounded" id="description" placeholder="Enter description..." autocomplete="off">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-success" id="submit">Update Data</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>