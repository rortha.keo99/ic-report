<div class="modal fade" id="ModalCreate" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">NEW CATEGORY</h4>
            </div>
            <div class="modal-body">
                <ul id="error_message"></ul>
                <ul id="success_message"></ul>
                <div class="form-group row">
                    <div class="col-md-6">
                        <label for="inputState">Category:</label>
                        <input type="text" class="form-control form-rounded category_name" placeholder="Enter category name..." autocomplete="off" required>
                    </div>
                    <div class="col-md-6">
                        <label for="inputZip">Description:</label>
                        <input type="text" class="form-control form-rounded description" placeholder="Enter description..." autocomplete="off">
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-success savebtnCategory">Save Data</button>
            </div>
        </div>
    </div>
</div>