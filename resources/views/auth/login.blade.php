<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<link rel="stylesheet" href="{{ asset('css/auth.css') }}" />
<div class="container">
    <div class="card card-container">
        <img id="profile-img" class="profile-img-card" src="{{ asset('images/logo/prasac_logo.png') }}" />
        <p id="profile-name" class="profile-name-card">PRASAC | E-Report</p>
        <form class="form-signin" method="POST" action="{{ route('login') }}">
            @csrf
            <span id="reauth-email" class="reauth-email"></span>
            @error('username')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
            <input id="username" type="text" class="form-control @error('username') is-invalid @enderror" name="username" value="{{ old('username') }}" required autocomplete="off" autofocus placeholder="User name or ID card">

            @error('password')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
            <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="off" placeholder="Password">
            <button class="btn btn-lg btn-primary btn-block btn-signin" type="submit">Sign in</button>
        </form><!-- /form -->
        @if (Route::has('register'))
        <a href="{{ route('register') }}">
            Don't have account?
        </a>
        @endif
    </div>
</div>