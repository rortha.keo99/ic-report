<div class="modal fade" id="ModalUpload" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">NEW FILE</h4>
            </div>
            <form method="POST" action="{{ url('/service/storeupload') }}" enctype="multipart/form-data">
                <div class="modal-body">
                    {{ csrf_field() }}
                    {{ method_field('POST') }}
                    <div class="form-group row">
                        <div class="col-md-6">
                            <label for="inputZip">Incident Code:</label>
                            <select class="form-control form-rounded" id="id" name="code_number_id" required>
                                <option selected disabled value="">-----SELECT-----</option>
                                @foreach (\App\Models\ServiceCheck::all() as $sc)
                                <option value="{{ $sc->id }}">{{ $sc->code_number }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-6">
                            <label for="formFileMultiple" class="form-label">File:</label>
                            <input class="form-control form-rounded" type="file" name="image" required multiple />
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-12">
                            <label for="inputState">Description:</label>
                            <textarea class="form-control form-rounded col-xs-12" rows="5" name="description" autocomplete="off" placeholder="Write your remark"></textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-success">Save Data</button>
                </div>
            </form>
        </div>
    </div>
</div>