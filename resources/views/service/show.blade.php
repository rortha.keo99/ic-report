<div class="modal fade" id="ModalShow" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">SHOW INFORMATION </h4>
            </div>
            <form method="POST">
                <div class="modal-body">
                    <table class="table table-bordered">
                        <tbody>
                            <tr>
                                <th colspan="4">General Information</th>
                            </tr>
                            <tr>
                                <th>Branch Name:</th>
                                <td colspan="3" id="sv_branch_id">
                                    @foreach (\App\Models\Branch::all() as $br)
                                    <p value="{{ $br->id }}">
                                        {{ $br->branch_code }}-{{ $br->branch_name }}
                                    </p>
                                    @endforeach
                                    <p></p>
                                </td>
                            </tr>
                            <tr>
                                <th>Incident title:</th>
                                <td colspan="3" id="sv_cause"></td>
                            </tr>
                            <tr>
                                <th>Contact Person:</th>
                                <td id="sv_contact_person"></td>
                                <th>Date:</th>
                                <td id="sv_created_at"></td>
                            </tr>
                        </tbody>
                    </table>
                    <table class="table table-bordered">
                        <tbody>
                            <tr>
                                <th colspan="3">Incident Report</th>
                                <th colspan="2">Incident ID:</th>
                                <td colspan="2" id="sv_code_number"></td>
                            </tr>
                            <tr>
                                <td colspan="7">
                                    <p>Model: <span id="sv_equip_name"></span></p>
                                    <p>Asset Code: <span id="sv_asset_code"></span>;<span id="sv_asset_previous_code"></span></p>
                                    <p>Purchase Date: <span id="sv_purchase_date"></span></p>
                                    <p>Service Tage: <span id="sv_service_tage"></span></p>
                                    <p>Condition: <span id="sv_condition_id"></span></p>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <table class="table table-bordered">
                        <tbody>
                            <tr>
                                <th colspan="7">IT Inspection report</th>
                            </tr>
                            <tr>
                                <td colspan="7" id="sv_inspection_check"></td>
                            </tr>
                        </tbody>
                    </table>
                    <table class="table table-bordered">
                        <tbody>
                            <tr>
                                <th colspan="7">List of part broken/replacement</th>
                            </tr>
                            <tr>
                                <th>No</th>
                                <th>Part Name</td>
                                <th colspan="2">Service Tag</th>
                                <th colspan="2">Old Serial No</th>
                                <th>New Serial No</th>
                            </tr>
                            <tr>
                                <td>
                                    <p>1.</p>
                                </td>
                                <td id="sv_part_name">&nbsp;</td>
                                <td colspan="2" id="sv_part_service_tag">&nbsp;</td>
                                <td colspan="2" id="sv_old_serial">&nbsp;</td>
                                <td id="sv_new_serial">&nbsp;</td>
                            </tr>
                            <tr>
                                <td>
                                    <p>2.</p>
                                </td>
                                <td id="sv_part_name">&nbsp;</td>
                                <td colspan="2" id="sv_part_service_tag">&nbsp;</td>
                                <td colspan="2" id="sv_old_serial">&nbsp;</td>
                                <td id="sv_new_serial">&nbsp;</td>
                            </tr>
                            <tr>
                                <td>
                                    <p>3.</p>
                                </td>
                                <td id="sv_part_name">&nbsp;</td>
                                <td colspan="2" id="sv_part_service_tag">&nbsp;</td>
                                <td colspan="2" id="sv_old_serial">&nbsp;</td>
                                <td id="sv_new_serial">&nbsp;</td>
                            </tr>
                            <tr>
                                <td>
                                    <p>4.</p>
                                </td>
                                <td id="sv_part_name">&nbsp;</td>
                                <td colspan="2" id="sv_part_service_tag">&nbsp;</td>
                                <td colspan="2" id="sv_old_serial">&nbsp;</td>
                                <td id="sv_new_serial">&nbsp;</td>
                            </tr>
                            <tr>
                                <td>
                                    <p>5.</p>
                                </td>
                                <td id="sv_part_name">&nbsp;</td>
                                <td colspan="2" id="sv_part_service_tag">&nbsp;</td>
                                <td colspan="2" id="sv_old_serial">&nbsp;</td>
                                <td id="sv_new_serial">&nbsp;</td>
                            </tr>
                        </tbody>
                    </table>
                    <table class="table table-bordered">
                        <tbody>
                            <tr>
                                <th>Remarks</th>
                            </tr>
                            <tr>
                                <td id="sv_remark"></td>
                            </tr>
                        </tbody>
                    </table>
                    <table class="table table-bordered">
                        <tbody>
                            <tr>
                                <th colspan="2">Engineer Signature</th>
                                <th colspan="2">Representative Signature</th>
                                <th colspan="2">Verify Signature</th>
                            </tr>
                            <!-- <tr>
                                <td colspan="2"><p>&nbsp;</p></td>  
                                <td colspan="2"><p>&nbsp;</p></td>
                                <td colspan="2"><p>&nbsp;</p></td> 
                            </tr> -->
                            <tr>
                                <th>Name:</p>
                                </th>
                                <td id="sv_responded_by"></td>
                                <th>Name:</th>
                                <td id="sv1_contact_person"></td>
                                <th>Name:</th>
                                <td id="sv_verified_by"></td>
                            </tr>
                            <tr>
                                <th>Date:</th>
                                <td id="sv1_created_at"></td>
                                <th>Date:</th>
                                <td id="sv2_created_at"></td>
                                <th>Date:</th>
                                <td id="sv3_created_at"></td>
                            </tr>
                        </tbody>
                    </table>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>