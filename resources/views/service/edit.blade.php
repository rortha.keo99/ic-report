<div class="modal fade" id="ModalEdit" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">UPDATE INFORMATION </h4>
            </div>
            <form method="POST" action="{{ url('service/update') }}">
                <div class="modal-body">
                    {{ csrf_field() }}
                    {{ method_field('put') }}
                    <input type="hidden" name="_method" value="PUT">
                    <input type="hidden" name="service_id" id="service_id">
                    <input type="hidden" name="code_number" id="code_number">
                    <div class="form-group row">
                        <div class="col-md-6">
                            <label for="inputState">Equip Name:</label>
                            <input type="text" class="form-control form-rounded" id="equip_name" name="equip_name" placeholder="Eneter equip name..." required autocomplete="off">
                        </div>
                        <div class="col-md-6">
                            <label for="inputZip">Cause:</label>
                            <input type="text" class="form-control form-rounded" id="cause" name="cause" placeholder="Enter cause..." required autocomplete="off">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-6">
                            <label for="inputZip">Asset Code:</label>
                            <input type="text" class="form-control form-rounded" id="asset_code" name="asset_code" placeholder="Enter asset code..." required autocomplete="off">
                        </div>
                        <div class="col-md-6">
                            <label for="inputZip">Previous Code:</label>
                            <input type="text" class="form-control form-rounded" id="asset_previous_code" name="asset_previous_code" placeholder="Enter previous code..." autocomplete="off">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-4">
                            <label for="inputState">Service Tage:</label>
                            <input type="text" class="form-control form-rounded" id="service_tage" name="service_tage" placeholder="Enter service tage..." autocomplete="off">
                        </div>
                        <div class="col-md-4">
                            <label for="inputZip">Category:</label>
                            <select class="form-control form-rounded" id="category_id" name="category_id" required>
                            <option selected disabled value="">-----SELECT-----</option>
                                @foreach (\App\Models\Category::all() as $cate)
                                <option value="{{ $cate->id }}">{{ $cate->category_name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-4">
                            <label for="inputZip">Branch Name:</label>
                            <select class="form-control form-rounded" id="branch_id" name="branch_id" required>
                            <option selected disabled value="">-----SELECT-----</option>
                                @foreach (\App\Models\Branch::all() as $br)
                                <option value="{{ $br->id }}">
                                    {{ $br->branch_code }}-{{ $br->branch_name }}
                                </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-4">
                            <label for="inputState">Purchase Date:</label>
                            <input type="date" class="form-control form-rounded" id="purchase_date" name="purchase_date">
                        </div>
                        <div class="col-md-4">
                            <label for="inputZip">Condition:</label>
                            <select class="form-control form-rounded" id="condition_id" name="condition_id" required>
                            <option selected disabled value="">-----SELECT-----</option>
                                @foreach (\App\Models\Condition::all() as $cond)
                                <option value="{{ $cond->id }}">{{ $cond->condition_name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-4">
                            <label for="inputZip">Contact Person:</label>
                            <input type="text" class="form-control form-rounded" id="contact_person" name="contact_person" placeholder="Enter contact person..." autocomplete="off" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-12">
                            <label for="inputState">IT Inspection:</label>
                            <textarea class="form-control form-rounded col-xs-12" rows="5" id="inspection_check" name="inspection_check" required autocomplete="off" placeholder="Write your inspectation"></textarea>
                        </div>
                    </div>
                    <div class="form-group row" id="row_field">
                        <div class="col-md-4">
                            <label for="inputState">Part Name:</label>
                            <input type="text" class="form-control form-rounded" id="part_name" name="part_name" placeholder="Enter part name..." autocomplete="off">
                        </div>
                        <div class="col-md-4">
                            <label for="inputZip">Service Tag:</label>
                            <input type="text" class="form-control form-rounded" id="part_service_tag" name="part_service_tag" placeholder="Enter service tage..." autocomplete="off">
                        </div>
                        <div class="col-md-2">
                            <label for="inputZip">Old Serial:</label>
                            <input type="text" class="form-control form-rounded" id="old_serial" name="old_serial" placeholder="Enter old serial..." autocomplete="off">
                        </div>
                        <div class="col-md-2">
                            <label for="inputZip">New Serial:</label>
                            <input type="text" class="form-control form-rounded" id="new_serial" name="new_serial" placeholder="Enter new serial..." autocomplete="off">
                        </div>
                        <!-- <div class="col-md-1">
                            <label for="inputState">Add</label>
                            <input type="button" class="btn btn-success" name="add" id="add" value="+">
                        </div> -->
                    </div>
                    <div class="form-group row">
                        <div class="col-md-12">
                            <label for="inputState">Remark:</label>
                            <textarea class="form-control form-rounded col-xs-12" rows="5" id="remark" name="remark" required autocomplete="off" placeholder="Write your remark"></textarea>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-4">
                            <label for="inputState">Responded By:</label>
                            <select class="form-control form-rounded" id="responded_by" name="responded_by" required>
                                <option selected>-----SELECT-----</option>
                                <option value="{{ Auth::user()->id}}">{{ Auth::user()->name }}</option>
                            </select>
                            <!-- <input type="text" class="form-control form-rounded" id="inputCity" name="responded_by" placeholder="Enter checker name" required autocomplete="off"> -->
                        </div>
                        <div class="col-md-4">
                            <label for="inputZip">Verified By:</label>
                            <input type="text" class="form-control form-rounded" id="verified_by" name="verified_by" placeholder="Verified by..." autocomplete="off" required>
                        </div>
                        <div class="col-md-4">
                            <label for="inputZip">Status:</label>
                            <select htmlattributes="{ class = form-control }" class="form-control form-rounded" id="statuses_id" name="statuses_id" required>
                            <option selected disabled value="">-----SELECT-----</option>
                                @foreach (\App\Models\Status::all() as $st)
                                <option value="{{ $st->id }}">{{ $st->status_name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-success" id="submit">Udate Data</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
