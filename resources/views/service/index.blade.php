@extends('admin.admin-master')
@section('content')
@section('title', 'Service')
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#service" data-toggle="tab">Service</a></li>
                    <li><a href="#fileupload" data-toggle="tab">File upload</a></li>
                </ul>
                <div class="tab-content">
                    <div class="active tab-pane" id="service">
                        <!-- Post -->
                        <div class="box box-primary">
                            <div class="box-header">
                                <h3 class="box-title">SERVICE INFORMATION</h3>
                                <button style="margin-bottom: 5px; margin-left: 15px" type="button" class="btn btn-success btn-xs" data-toggle="modal" data-target="#ModalCreate">New Information</button>
                            </div>
                            @if($errors -> count() > 0)
                            <div class="alert error-message" style=" list-style: none;">
                                @foreach($errors->all() as $error)
                                <li>
                                    {{ $error }}
                                </li>
                                @endforeach
                            </div>
                            @endif
                            <div class="box-body table-responsive">
                                @if (isset($service))
                                <table class="ui celled table responsive nowrap table-sm table-hover" style="width:10" id="dataTable">
                                    <thead>
                                        <tr>
                                            <th>N<sup>O</sup></th>
                                            <th>CODE</th>
                                            <th>BRANCH</th>
                                            <th>EQUIP</th>
                                            <th>CAUSE</th>
                                            <th>ASSET</th>
                                            <th>TYPE</th>
                                            <th>CONTACT</th>
                                            <th>RESPONED</th>
                                            <th>STATUS</th>
                                            <th>DATE</th>
                                            <th>ACT</th>
                                            <th><input type="checkbox" id="check-all"></th>
                                        </tr>
                                    </thead>
                                    @if(count($service) > 0)
                                    @foreach ($service as $key => $sc)
                                    <tr>
                                        <td>{{ ++$key }}</td>
                                        <td>{{ $sc->code_number ?? 'NULL'}}</td>
                                        <td>{{ $sc->Branch->branch_code ?? 'NULL' }}-{{ $sc->Branch->branch_name ?? 'NULL'}}</td>
                                        {{-- <td>{{ Illuminate\Support\Str::of($sc->equip_name)->words(2) ?? 'NULL'}}</td> --}}
                                        <td>{{ $sc->equip_name ?? 'NULL'}}</td>
                                        <td>{{ Illuminate\Support\Str::of($sc->cause)->words(4) ?? 'NULL' }}</td>
                                        <td><a href="{{ url('/service/export/'.$sc->id)}}">{{ $sc->asset_code ?? 'NULL'}}</a></td>
                                        <td>{{ $sc->Category->category_name ?? 'NULL' }}</td>
                                        <td>{{ $sc->contact_person ?? 'NULL'}}</td>
                                        <td>{{ $sc->User->name ?? 'None'}}</td>
                                        <td>{{ $sc->Status->status_name ?? 'NULL'}}</td>
                                        <td>{{ date('d.m.Y', strtotime($sc->created_at)) }}</td>
                                        <td>
                                            <button value="{{ $sc-> id }}" class="btn btn-success btn-xs rounded-0 viewbtnService" type="button" data-toggle="tooltip" data-placement="top" title="View"><i class="fa fa-eye"></i></button>
                                            <button value="{{ $sc-> id }}" class="btn btn-success btn-xs rounded-0 editbtnService" data-toggle="tooltip" type="button" data-placement="top" title="Edit"><i class="fa fa-edit"></i></button>
                                            <div class="btn-group dropstart">
                                                <button class="btn btn-success btn-xs rounded-0 dropdown-toggle" type="button" data-toggle="dropdown">
                                                    More option
                                                </button>
                                                <ul class="dropdown-menu">
                                                    <li><a href="{{ url('/service/export/'.$sc->id)}}" class="download-confirm">Export word</a></li>
                                                    <li><a href="{{ url('/service/delete/'. $sc->id) }}" class="delete-confirm" type="submit">Delete</a></li>
                                                </ul>
                                            </div>
                                        </td>
                                        <th><input type="checkbox" name="checkbox"></th>
                                    </tr>
                                    @endforeach
                                    @endif
                                </table>
                                @endif
                            </div>
                        </div>
                    </div>
                    <!-- /.tab-pane -->
                    <div class="tab-pane" id="fileupload">
                        <!-- The timeline -->
                        <div class="tab-content">
                            <div class="active tab-pane" id="service">
                                <!-- Post -->
                                <div class="box box-primary">
                                    <div class="box-header">
                                        <h3 class="box-title">FILE</h3>
                                        <button style="margin-bottom: 5px; margin-left: 15px" type="button" class="btn btn-success btn-xs" data-toggle="modal" data-target="#ModalUpload">New File</button>
                                    </div>
                                    <div class="our-webcoderskull padding-lg">
                                        <ul class="row">
                                            @if(count($upload) > 0)
                                            @foreach($upload as $key => $up)
                                            <li class="col-12 col-md-6 col-lg-2">
                                                <div class="cnt-block" style="height: 230px;">
                                                    <figure><i class="bi-file-earmark-pdf"></i></i></figure>
                                                    <h3><a href="/service">{{ $up -> Service -> code_number ?? 'NULL'}}</a></h3>
                                                    <p> {{ $up -> description ?? 'NULL'}} </p>
                                                    <a href="uploads/service/{{ $up -> image}}" download="{{ $up -> image}}" class="btn btn-success">
                                                        <span class="glyphicon glyphicon-download-alt"></span>Download
                                                    </a>
                                                </div>
                                            </li>
                                            @endforeach
                                            @endif
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('service.create')
    @include('service.edit')
    @include('service.show')
    @include('service.upload')
</section>
@endsection