<div class="modal fade" id="ModalCreate" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">NEW INFORMATION</h4>
            </div>
            <!-- <form method="POST" action="{{ url('/service/save') }}"> -->
            <div class="modal-body">
                <ul id="error_message"></ul>
                <ul id="success_message"></ul>
                <div class="form-group row">
                    <div class="col-md-6">
                        <label for="inputState">Equip Name:</label>
                        <input type="text" class="form-control form-rounded equip_name" placeholder="Eneter equip name..." autocomplete="off">
                    </div>
                    <div class="col-md-6">
                        <label for="inputZip">Cause:</label>
                        <input type="text" class="form-control form-rounded cause" placeholder="Enter cause..." autocomplete="off">
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-6">
                        <label for="inputZip">Asset Code:</label>
                        <input type="text" class="form-control form-rounded asset_code" placeholder="Enter asset code..." autocomplete="off">
                    </div>
                    <div class="col-md-6">
                        <label for="inputZip">Previous Code:</label>
                        <input type="text" class="form-control form-rounded asset_previous_code" placeholder="Enter previous code..." autocomplete="off">
                    </div>
                </div>
                <div class="form-group row">
                    <div class="form-group col-md-4">
                        <label for="inputState">Service Tage:</label>
                        <input type="text" class="form-control form-rounded service_tage" placeholder="Enter service tage..." autocomplete="off">
                    </div>
                    <div class="form-group col-md-4">
                        <label for="inputZip">Category:</label>
                        <select class="form-control form-rounded category_id">
                            <option selected disabled value="">-----SELECT-----</option>
                            @foreach (\App\Models\Category::all() as $cate)
                            <option value="{{ $cate->id }}">{{ $cate->category_name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group col-md-4">
                        <label for="inputZip">Branch Name:</label>
                        <select class="form-control form-rounded branch_id">
                            <option selected disabled value="">-----SELECT-----</option>
                            @foreach (\App\Models\Branch::all() as $br)
                            <option value="{{ $br->id }}">
                                {{ $br->branch_code }}-{{ $br->branch_name }}
                            </option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="form-group col-md-4">
                        <label for="inputState">Purchase Date:</label>
                        <input type="date" class="form-control form-rounded purchase_date">
                    </div>
                    <div class="form-group col-md-4">
                        <label for="inputZip">Condition:</label>
                        <select class="form-control form-rounded condition_id">
                            <option selected disabled value="">-----SELECT-----</option>
                            @foreach (\App\Models\Condition::all() as $cond)
                            <option value="{{ $cond->id }}">{{ $cond->condition_name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group col-md-4">
                        <label for="inputZip">Contact Person:</label>
                        <input type="text" class="form-control form-rounded contact_person" placeholder="Enter contact person..." autocomplete="off">
                    </div>
                </div>
                <div class="form-group row">
                    <div class="form-group col-md-12">
                        <label for="inputState">IT Inspection:</label>
                        <textarea class="form-control form-rounded col-xs-12 inspection_check" rows="5" autocomplete="off" placeholder="Write your inspectation"></textarea>
                    </div>
                </div>
                <div class="form-group row" id="row_field">
                    <div class="form-group col-md-4">
                        <label for="inputState">Part Name:</label>
                        <input type="text" class="form-control form-rounded part_name" placeholder="Enter part name..." autocomplete="off">
                    </div>
                    <div class="form-group col-md-4">
                        <label for="inputZip">Service Tag:</label>
                        <input type="text" class="form-control form-rounded part_service_tag" placeholder="Enter service tage..." autocomplete="off">
                    </div>
                    <div class="form-group col-md-2">
                        <label for="inputZip">Old Serial:</label>
                        <input type="text" class="form-control form-rounded old_serial" placeholder="Enter old serial..." autocomplete="off">
                    </div>
                    <div class="form-group col-md-2">
                        <label for="inputZip">New Serial:</label>
                        <input type="text" class="form-control form-rounded new_serial" placeholder="Enter new serial..." autocomplete="off">
                    </div>
                    <!-- <div class="form-group col-md-1">
                        <label for="inputState">Add</label>
                        <input type="button" class="btn btn-success" name="add" id="add" value="+">
                    </div> -->
                </div>
                <div class="form-group row">
                    <div class="form-group col-md-12">
                        <label for="inputState">Remark:</label>
                        <textarea class="form-control form-rounded col-xs-12 remark" rows="5" autocomplete="off" placeholder="Write your remark"></textarea>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="form-group col-md-4">
                        <label for="inputState">Responded By:</label>
                        <select class="form-control form-rounded responded_by">
                            <option selected disabled value="">-----SELECT-----</option>
                            <option value="{{ Auth::user()->id ?? 'NULL' }}">{{ Auth::user()->name ?? 'NULL' }}</option>
                        </select>
                    </div>
                    <div class="form-group col-md-4">
                        <label for="inputZip">Verified By:</label>
                        <input type="text" class="form-control form-rounded verified_by" placeholder="Verified by..." autocomplete="off">
                    </div>
                    <div class="form-group col-md-4">
                        <label for="inputZip">Status:</label>
                        <select class="form-control form-rounded statuses_id">
                            <option selected disabled value="">-----SELECT-----</option>
                            @foreach (\App\Models\Status::all() as $st)
                            <option value="{{ $st->id }}">{{ $st->status_name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-success savebtnService">Save Data</button>
            </div>
            <!-- </form> -->
        </div>
    </div>
</div>