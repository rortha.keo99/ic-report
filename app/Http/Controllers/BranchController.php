<?php

namespace App\Http\Controllers;

use App\Models\Branch;
use App\Http\Requests\StoreBranchRequest;
use App\Http\Requests\UpdateBranchRequest;
use Illuminate\Support\Facades\Validator;

class BranchController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $branch = Branch::orderBy('id', 'desc')->get();
        return view('branch.index', compact('branch'));
    }

    public function create()
    {
        return view('branch.create');
    }

    public function store(StoreBranchRequest $request)
    {
        $rule = [
            "branch_code" => "required|numeric",
            "branch_name" => "required"
        ];

        $validator = Validator::make($request->all(), $rule);
        if ($validator->fails()) {
            return response()->json([
                'status' => 400,
                'errors' => $validator->messages(),
            ]);
        } else {
            $branch = new Branch();
            $branch->branch_code = $request->input('branch_code');
            $branch->branch_name = $request->input('branch_name');
            $branch->description = $request->input('description');
            $branch->save();
            return response()->json([
                'status' => 200,
                'message' => 'Data Saved!',
            ]);
        }
    }

    public function show(Branch $branch)
    {
        //
    }

    public function edit($id)
    {
        $branch = Branch::find($id);
        return response()->json([
            'status' => 200,
            'branch' => $branch
        ]);
    }

    public function update(UpdateBranchRequest $request)
    {
        if ($this->validate($request, [
            'branch_code' => 'required|numeric',
            'branch_name' => 'required'
        ]));
        $branch_id = $request->input('branch_id');
        $branch = Branch::find($branch_id);
        $branch->branch_code = $request->input('branch_code');
        $branch->branch_name = $request->input('branch_name');
        $branch->description = $request->input('description');
        $branch->update();
        return redirect()->back()->with('update', '');
    }

    public function destroy($id)
    {
        Branch::where('id', $id)->delete();
        return redirect()->route('branch')->with('delete', '');
    }
}
