<?php

namespace App\Http\Controllers;

use App\Models\Condition;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ConditionController extends Controller
{
    public function index()
    {
        $condition = Condition::orderBy('id', 'desc')->get();
        return view('condition.index', compact('condition'));
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        $rule = [
            "condition_name" => "required"
        ];
        $validator = Validator::make($request->all(), $rule);
        if ($validator->fails()) {
            return response()->json([
                'status' => 400,
                'errors' => $validator->messages(),
            ]);
        } else {
            $condition = new Condition();
            $condition->condition_name = $request->condition_name;
            $condition->description = $request->description;
            $condition->save();
            return response()->json([
                'status' => 200,
                'message' => 'Data Saved!',
            ]);
        }
    }

    public function show(Condition $condition)
    {
        //
    }

    public function edit($id)
    {
        $condition = Condition::find($id);
        return response()->json([
            'status' => 200,
            'condition' => $condition
        ]);
    }

    public function update(Request $request, Condition $condition)
    {
        if ($this->validate($request, [
            'condition_name' => 'required',
        ]));
        $condition_id = $request->input('condition_id');
        $condition = Condition::find($condition_id);
        $condition->condition_name = $request->input('condition_name');
        $condition->description = $request->input('description');
        $condition->update();
        return redirect()->back()->with('update', '');
    }

    public function destroy($id)
    {
        Condition::where('id', $id)->delete();
        return redirect()->route('condition')->with('delete', '');
    }
}
