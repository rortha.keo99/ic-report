<?php

namespace App\Http\Controllers;

use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\Models\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $product = Product::with('ProductType', 'Unit', 'Status', 'User')->orderBy('id', 'desc')->get();
        return view('admin.product.index', compact('product'));
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        if($this->validate($request, [
            'product_name' => 'required',
            'product_type' => 'integer',
            'quantity' => 'required|numeric',
            'unit_id' => 'required|integer',
            'product_status_id' => 'required|integer',
            'responder_id' => 'required|integer',
        ]));
        $product = new Product();
        $product -> code_item = Helper::CODEGenerator(new Product, 'code_item', 5, 'CODE');
        $product -> barcode = $request -> barcode;
        $product -> product_name = $request -> product_name;
        $product -> product_type = $request -> product_type;
        $product -> quantity = $request -> quantity;
        $product -> unit_id = $request -> unit_id;
        $product -> product_price = $request -> product_price;
        $product -> currency_id = $request -> currency_id;
        $product -> product_status_id = $request -> product_status_id;
        $product -> responder_id = $request -> responder_id;
        $product -> description = $request -> description;

        if($request->hasFile('image')){
            $file = $request->file('image');
            $extension = $file->getClientOriginalExtension();
            $filename = time().'.'.$extension;
            $file->move('uploads/product/', $filename);
            $product -> image = $filename;
        }
        $product -> save();
        return redirect()->back()->with('save', '');
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $product = Product::find($id);
        return response()->json([
            'status' => 200,
            'product' => $product
        ]);
    }

    public function update(Request $request)
    {
        if($this->validate($request, [
            'product_name' => 'required',
            'product_type' => 'integer',
            'quantity' => 'required|numeric',
            'unit_id' => 'required|integer',
            'product_price' => 'numeric',
            'currency_id' => 'required|integer',
            'product_status_id' => 'required|integer',
            'responder_id' => 'required|integer',

        ]));
        $product_id = $request -> input('product_id');
        $product = Product::find($product_id);
        $product -> barcode = $request -> input('barcode');
        $product -> product_name = $request -> input('product_name');
        $product -> product_type = $request -> input('product_type');
        $product -> quantity = $request -> input('quantity');
        $product -> unit_id = $request -> input('unit_id');
        $product -> product_price = $request -> input('product_price');
        $product -> currency_id = $request -> input('currency_id');
        $product -> product_status_id = $request -> input('product_status_id');
        $product -> responder_id = $request -> input('responder_id');
        $product -> description = $request -> input('description');
        $product -> update();
        return redirect()->back()->with('update', '');
    }

    public function destroy($id)
    {
        Product::where('id', $id)->delete();
        return redirect()->route('product')->with('delete', '');
    }
}
