<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\ServiceCheck;
use App\Models\User;

class DashboardController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        $count_user = User::count();
        $count_service = ServiceCheck::count();
        $count_complete = ServiceCheck::where('statuses_id', '1')->count();
        $count_repairing = ServiceCheck::where('statuses_id', '3')->count();
        $count_checking = ServiceCheck::where('statuses_id', '5')->count();
        $count_sendtoservice = ServiceCheck::where('statuses_id', '2')->count();
        $count_retire = ServiceCheck::where('statuses_id','4')->count();
        $count_product = Product::count();
        
        
        return view('admin.dashboard.index', compact('count_user', 'count_service', 'count_complete','count_retire', 'count_checking', 'count_repairing', 'count_sendtoservice', 'count_product'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
