<?php

namespace App\Http\Controllers;

use App\Models\Report;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ReportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $report = Report::orderBy('id', 'desc')->get();
        return view('report.index', compact('report'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($this->validate($request, [
            'kh_name' => 'required',
            'eng_name' => 'required',
            'image' => 'required|mimes:pdf,xlsx,docx,csv|max:2048',
        ]));
        $report = new Report();
        $report -> kh_name = $request -> kh_name;
        $report -> eng_name = $request -> eng_name;
        $report -> description = $request -> description;
        if($request->hasFile('image')){
            $file = $request->file('image');
            $extension = $file->getClientOriginalExtension();
            $filename = time().'.'.$extension;
            $file->move('uploads/report/', $filename);
            $report -> image = $filename;
        }
        $report -> save();
        return redirect()->back()->with('save', '');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Report  $report
     * @return \Illuminate\Http\Response
     */
    public function show(Report $report)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Report  $report
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $report = Report::find($id);
        return response()->json([
            'status' => 200,
            'report' => $report
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Report  $report
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        if($this->validate($request, [
            'kh_name' => 'required',
            'eng_name' => 'required',
        ]));
        
        $report_id = $request -> input('report_id');
        $report = Report::find($report_id);
        $report->kh_name = $request->input('kh_name');
        $report->description = $request->input('description');
        $report->update();
        return redirect()->back()->with('update', '');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Report  $report
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Report::where('id', $id)->delete();
        return redirect()->route('report')->with('delete', '');
    }
}
