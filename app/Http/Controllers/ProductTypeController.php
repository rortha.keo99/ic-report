<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\ProductType;
use Illuminate\Http\Request;

class ProductTypeController extends Controller
{

    public function index()
    {
        $producttype = ProductType::orderBy('id', 'desc')->get();
        return view('admin.product-type.index', compact('producttype'));
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        if($this->validate($request, [
            'type_name' => 'required',
        ]));
        $pro = new ProductType();
        $pro -> type_name = $request -> type_name;
        $pro -> description = $request -> description;
        if($request->hasFile('image')){
            $file = $request->file('image');
            $extension = $file->getClientOriginalExtension();
            $filename = time().'.'.$extension;
            $file->move('uploads/product-type/', $filename);
            $pro -> image = $filename;
        }
        $pro -> save();
        return redirect()->back()->with('save', '');
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $pro = ProductType::find($id);
        return response()->json([
            'status' => 200,
            'protype' => $pro,
        ]);
    }

    public function update(Request $request)
    {
        if($this->validate($request, [
            'type_name' => 'required',
        ]));
        $protype_id = $request->input('protype_id');
        $pro = ProductType::find($protype_id);
        $pro->type_name = $request->input('type_name'?? 'NULL');
        $pro->description = $request->input('description'?? 'NULL');
        $pro->update();
        return redirect()->back()->with('update', '');
    }

    public function destroy($id)
    {
        ProductType::where('id', $id)->delete();
        return redirect()->route('protype')->with('delete', '');
    }
}
