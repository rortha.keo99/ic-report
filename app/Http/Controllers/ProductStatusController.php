<?php

namespace App\Http\Controllers;

use App\Models\ProductStatus;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ProductStatusController extends Controller
{
    public function index()
    {
        $prostatus = ProductStatus::orderBy('id', 'desc')->get();
        return view('admin.product-status.index', compact('prostatus'));
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        $rule = [
            "pro_status" => "required",
        ];

        $validator = Validator::make($request->all(), $rule);
        if ($validator->fails()) {
            return response()->json([
                'status' => 400,
                'errors' => $validator->messages(),
            ]);
        } else {
            $prostatus = new ProductStatus();
            $prostatus->pro_status = $request-> input('pro_status');
            $prostatus->description = $request->input('description');
            $prostatus->save();
            return response()->json([
                'status' => 200,
                'message' => 'Data Saved!',
            ]);
        }
    }

    public function show(ProductStatus $productStatus)
    {
        //
    }

    public function edit($id)
    {
        $prostatus = ProductStatus::find($id);
        return response()->json([
            'status' => 200,
            'prostatus' => $prostatus
        ]);
    }

    public function update(Request $request)
    {
        if ($this->validate($request, [
            'pro_status' => 'required',
        ]));
        $prostatus_id = $request->input('prostatus_id');
        $prostatus = ProductStatus::find($prostatus_id);
        $prostatus->pro_status = $request->input('pro_status');
        $prostatus->description = $request->input('description');
        $prostatus->update();
        return redirect()->back()->with('update', '');
    }

    public function destroy($id)
    {
        ProductStatus::where('id', $id)->delete();
        return redirect()->route('prostatus')->with('delete', '');
    }
}
