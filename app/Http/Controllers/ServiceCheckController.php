<?php

namespace App\Http\Controllers;

use App\Models\ServiceCheck;
use App\Http\Requests\StoreServiceCheckRequest;
use Illuminate\Http\Request;
use App\Models\Category;
use App\Helpers\Helper;
use App\Models\FileUpload;
use PhpOffice\PhpWord\TemplateProcessor;
use Illuminate\Support\Facades\Validator;

class ServiceCheckController extends Controller
{
    public function index()
    {
        $service = ServiceCheck::with('Category', 'Branch', 'Condition', 'Status', 'User', 'FileUpload')->orderBy('id', 'desc')->get();
        $upload = FileUpload::with('Service')->orderBy('id', 'desc')->get();
        return view('service.index', compact('service', 'upload'));
    }

    public function create(Request $request, $id)
    {
        $categories = Category::with('service_checks')->find($id);
        $categories = collect($categories)->where('category_name');
        return view('service.create', compact('categories'));
    }

    public function store(StoreServiceCheckRequest $request)
    {
        $rule = [
            "equip_name" => "required",
            "cause" => "required",
            "asset_code" => "required",
            "category_id" => "required",
            "branch_id" => "required",
            "condition_id" => "required",
            "contact_person" => "required",
            "inspection_check" => "required",
            "remark" => "required",
            "responded_by" => "required",
            "statuses_id" => "required",
        ];

        $validator = Validator::make($request->all(), $rule);
        if ($validator->fails()) {
            return response()->json([
                'status' => 400,
                'errors' => $validator->messages(),
                // 'errors' => $validator->messages(),
            ]);
        } else {
            $service = new ServiceCheck();
            $service->equip_name = $request->equip_name;
            $service->cause = $request->cause;
            $service->code_number = Helper::CODEGenerator(new ServiceCheck, 'code_number', 5, 'ITS');
            $service->asset_code = $request->asset_code;
            $service->asset_previous_code = $request->asset_previous_code;
            $service->service_tage = $request->service_tage;
            $service->category_id = $request->category_id;
            $service->branch_id = $request->branch_id;
            $service->inspection_check = str_replace(' ', ' ', $request->inspection_check);
            $service->responded_by = $request->responded_by;
            $service->verified_by = $request->verified_by;
            $service->part_name = $request->part_name;
            $service->part_service_tag = $request->part_service_tag;
            $service->old_serial = $request->old_serial;
            $service->new_serial = $request->new_serial;
            $service->purchase_date = $request->purchase_date;
            $service->contact_person = $request->contact_person;
            $service->condition_id = $request->condition_id;
            $service->remark = $request->remark;
            $service->statuses_id = $request->statuses_id;
            $service->save();
            return response()->json([
                'status' => 200,
                'message' => 'Data Saved!',
            ]);
        }
    }

    public function show($id)
    {
        $service = ServiceCheck::find($id);
        return response()->json([
            'status' => 200,
            'service' => $service
        ]);
    }

    public function edit($id)
    {
        $service = ServiceCheck::find($id);
        return response()->json([
            'status' => 200,
            'service' => $service
        ]);
    }

    public function update(Request $request)
    {
        if ($this->validate($request, [
            'equip_name' => 'required',
            'cause' => 'required',
            'asset_code' => 'required',
            'category_id' => 'required',
            'branch_id' => 'required',
            'inspection_check' => 'required',
            'responded_by' => 'required',
            'contact_person' => 'required',
            'condition_id' => 'required',
            'remark' => 'required',
            'statuses_id' => 'required',
            'verified_by' => 'required',
        ]));
        $service_id = $request->input('service_id');
        $service = ServiceCheck::find($service_id);
        $service->equip_name = $request->input('equip_name');
        $service->cause = $request->input('cause');
        $service->asset_code = $request->input('asset_code');
        $service->asset_previous_code = $request->input('asset_previous_code');
        $service->service_tage = $request->input('service_tage');
        $service->category_id = $request->input('category_id');
        $service->branch_id = $request->input('branch_id');
        $service->purchase_date = $request->input('purchase_date');
        $service->condition_id = $request->input('condition_id');
        $service->contact_person = $request->input('contact_person');
        $service->inspection_check = $request->input('inspection_check');
        $service->part_name = $request->input('part_name');
        $service->part_service_tag = $request->input('part_service_tag');
        $service->old_serial = $request->input('old_serial');
        $service->new_serial = $request->input('new_serial');
        $service->remark = $request->input('remark');
        $service->responded_by = $request->input('responded_by');
        $service->verified_by = $request->input('verified_by');
        $service->statuses_id = $request->input('statuses_id');
        if ($request->hasFile('file_upload')) {
            $destination_path = 'public/fileupload';
            $file = $request->file('file_upload');
            $filename = $file->getClientOriginalExtension();
            $path = $request->file('file_upload')->storeAs($destination_path, $filename);
            $service->file_upload = $request->input($filename);
        }
        $service->update();
        return redirect()->back()->with('update', '');
    }

    public function destroy($id)
    {
        ServiceCheck::where('id', $id)->delete();
        return redirect()->route('service')->with('delete', '');
    }

    public function wordexport($id)
    {
        $service = ServiceCheck::findOrFail($id);
        $templateProcessor = new TemplateProcessor('word-template/service-check.docx');
        $templateProcessor->setValue('id', $service->id);
        $templateProcessor->setValue('equip_name', $service->equip_name);
        $templateProcessor->setValue('cause', $service->cause);
        $templateProcessor->setValue('code_number', $service->code_number);
        $templateProcessor->setValue('asset_code', $service->asset_code);
        $templateProcessor->setValue('asset_previous_code', $service->asset_previous_code);
        $templateProcessor->setValue('service_tage', $service->service_tage);
        $templateProcessor->setValue('category_id', $service->category_id);
        $templateProcessor->setValue('branch_code', $service->Branch->branch_code ?? 'NULL');
        $templateProcessor->setValue('branch_name', $service->Branch->branch_name ?? 'NULL');
        $templateProcessor->setValue('inspection_check', $service->inspection_check);
        $templateProcessor->setValue('responded_by', $service->User->name ?? 'NULL');
        $templateProcessor->setValue('verified_by', $service->verified_by);
        $templateProcessor->setValue('part_name', $service->part_name);
        $templateProcessor->setValue('part_service_tag', $service->part_service_tag);
        $templateProcessor->setValue('old_serial', $service->old_serial);
        $templateProcessor->setValue('new_serial', $service->new_serial);
        $templateProcessor->setValue('purchase_date', date('d-m-Y', strtotime($service->purchase_date)));
        $templateProcessor->setValue('contact_person', $service->contact_person);
        $templateProcessor->setValue('condition_id', $service->Condition->condition_name ?? 'NULL');
        $templateProcessor->setValue('remark', $service->remark);
        $templateProcessor->setValue('statuses_id', $service->statuses_id);
        $templateProcessor->setValue('created_at', date('d-m-Y', strtotime($service->created_at)));
        $filename = $service->code_number . '_' . 'FC' . ($service->Branch->branch_code ?? 'NULL') . '_' . $service->equip_name . '_' . $service->cause . '_' . $service->contact_person;
        $templateProcessor->saveAs($filename . '.docx');
        return response()->download($filename . '.docx')->deleteFileAfterSend(true);
    }
}
