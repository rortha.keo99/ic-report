<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Http\Requests\StoreCategoryRequest;
use App\Http\Requests\UpdateCategoryRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CategoryController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $categories = Category::orderBy('id', 'desc')->get();
        return view('category.index', compact('categories'));
    }

    public function create()
    {
        return view('category.create');
    }

    public function store(StoreCategoryRequest $request)
    {
        $rule = [
            "category_name" => "required"
        ];
        $validator = Validator::make($request->all(), $rule);
        if ($validator->fails()) {
            return response()->json([
                'status' => 400,
                'errors' => $validator->messages(),
            ]);
        } else {
            $categories = new Category();
            $categories->category_name = $request->input('category_name');
            $categories->description = $request->input('description');
            $categories->save();
            return response()->json([
                'status' => 200,
                'message' => 'Data Saved!',
            ]);
        }
    }

    public function show(Category $category)
    {
        //
    }

    public function edit($id)
    {
        $categories = Category::find($id);
        return response()->json([
            'status' => 200,
            'category' => $categories,
        ]);
    }

    public function update(UpdateCategoryRequest $request)
    {
        if ($this->validate($request, [
            'category_name' => 'required',
        ]));
        $cate_id = $request->input('cate_id');
        $categories = Category::find($cate_id);
        $categories->category_name = $request->input('category_name');
        $categories->description = $request->input('description');
        $categories->update();
        return redirect()->back()->with('update', '');
    }

    public function destroy($id)
    {
        Category::where('id', $id)->delete();
        return redirect()->route('category')->with('delete', '');
    }

    public function deleteAll(Request $request)
    {
    }
}
