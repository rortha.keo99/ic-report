<?php

namespace App\Http\Controllers;

use App\Models\FileUpload;
use App\Http\Controllers\Controller;
use App\Models\ServiceCheck;
use Illuminate\Http\Request;

class FileUploadController extends Controller
{

    public function index()
    {
        $upload = FileUpload::orderBy('id', 'desc')->get();
        return view('service.index', compact('upload'));
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        $request->validate([
            'image' => 'required|mimes:pdf|max:10000'
        ]);

        $upload = new FileUpload();
        $upload -> code_number_id = $request -> code_number_id;
        $upload -> description = $request -> description;

        if($request->hasFile('image')){
            $file = $request->file('image');
            $exten = $file->getClientOriginalExtension();
            $namefile = time().'.'.$exten;
            $file->move('uploads/service/', $namefile);
            $upload->image = $namefile;
        }
        $upload -> save();
        return redirect()->back()->with('save', '');
    }

    public function show(FileUpload $fileUpload)
    {
        //
    }

    public function edit(FileUpload $fileUpload)
    {
        //
    }

    public function update(Request $request, FileUpload $fileUpload)
    {
        //
    }

    public function destroy(FileUpload $fileUpload)
    {
        //
    }

    public function ServiceCheck(){
        return $this->hasMany(ServiceCheck::class);
    }
}
