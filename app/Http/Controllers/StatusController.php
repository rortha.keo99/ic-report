<?php

namespace App\Http\Controllers;

use App\Models\Status;
use App\Http\Requests\StoreStatusRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class StatusController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $status = Status::orderBy('id', 'desc')->get();
        return view('status.index', compact('status'));
    }

    public function create()
    {
        return view('status.create');
    }

    public function store(StoreStatusRequest $request)
    {
        $rule = [
            "status_name" => "required",
        ];

        $validator = Validator::make($request->all(), $rule);
        if ($validator->fails()) {
            return response()->json([
                'status' => 400,
                'errors' => $validator->messages(),
            ]);
        } else {
            $status = new Status();
            $status->status_name = $request->input('status_name');
            $status->description = $request->input('description');
            $status->save();
            return response()->json([
                'status' => 200,
                'message' => 'Data Saved!',
            ]);
        }
    }

    public function show(Status $status)
    {
        //
    }

    public function edit($id)
    {
        $status = Status::find($id);
        return response()->json([
            'status' => 200,
            'status' => $status
        ]);
    }

    public function update(Request $request)
    {
        if ($this->validate($request, [
            'status_name' => 'required',
        ]));
        $status_id = $request->input('status_id');
        $status = Status::find($status_id);
        $status->status_name = $request->input('status_name');
        $status->description = $request->input('description');
        $status->update();
        return redirect()->back()->with('update', '');
    }

    public function destroy($id)
    {
        Status::where('id', $id)->delete();
        return redirect()->route('status')->with('delete', '');
    }
}
