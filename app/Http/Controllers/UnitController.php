<?php

namespace App\Http\Controllers;

use App\Models\Unit;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class UnitController extends Controller
{
    public function index()
    {
        $unit = Unit::orderBy('id', 'desc')->get();
        return view('admin.unit.index', compact('unit'));
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        $rule = [
            "unit_name" => "required",
        ];

        $validator = Validator::make($request->all(), $rule);
        if ($validator->fails()) {
            return response()->json([
                'status' => 400,
                'errors' => $validator->messages(),
            ]);
        } else {
            $unit = new Unit();
            $unit->unit_name = $request->unit_name;
            $unit->description = $request->description;
            $unit->save();
            return response()->json([
                'status' => 200,
                'message' => 'Data Saved!',
            ]);
        }
    }

    public function show(Unit $unit)
    {
        //
    }

    public function edit($id)
    {
        $unit = Unit::find($id);
        return response()->json([
            'status' => 200,
            'unit' => $unit
        ]);
    }

    public function update(Request $request)
    {
        if ($this->validate($request, [
            'unit_name' => 'required'
        ]));
        $unit_id = $request->input('unit_id');
        $unit = Unit::find($unit_id);
        $unit->unit_name = $request->unit_name;
        $unit->description = $request->description;
        $unit->update();
        return redirect()->back()->with('update', '');
    }

    public function destroy($id)
    {
        Unit::where('id', $id)->delete();
        return redirect()->route('unit')->with('delete', '');
    }
}
