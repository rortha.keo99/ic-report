<?php

namespace App\Http\Controllers;

use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\Models\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class OrderController extends Controller
{

    public function index()
    {
        $order = Order::with('Product', 'Branch', 'Unit', 'Responder')->orderBy('id', 'desc')->get();
        return view('admin.order.index', compact('order'));
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        $rule = [
            "order_name" => "required",
            "branch_id" => "required",
            "product_name_id" => "required",
            "order_quantity" => "required",
            "responder_id" => "required",
        ];

        $validator = Validator::make($request->all(), $rule);
        if ($validator->fails()) {
            return response()->json([
                'status' => 400,
                'errors' => $validator->messages(),
            ]);
        } else {
            $order = new Order();
            $order->code_order = Helper::CODEGenerator(new Order, 'code_order', 5, 'ORDER');
            $order->order_name = $request->order_name;
            $order->phone = $request->phone;
            $order->branch_id = $request->branch_id;
            $order->product_name_id = $request->product_name_id;
            $order->order_quantity = $request->order_quantity;
            $order->description = $request->description;
            $order->responder_id = $request->responder_id;
            $order->save();
            return response()->json([
                'status' => 200,
                'message' => 'Data Saved!',
            ]);
        }
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $order = Order::find($id);
        return response()->json([
            'status' => 200,
            'order' => $order
        ]);
    }

    public function update(Request $request)
    {
        if ($this->validate(
            $request,
            [
                'order_name' => 'required',
                'branch_id' => 'required|integer',
                'product_name_id' => 'required|integer',
                'phone' => 'numeric',
                'order_quantity' => 'numeric',
                'responder_id' => 'required',
            ]
        ));
        $order_id = $request->input('order_id');
        $order = Order::find($order_id);
        $order->order_name = $request->input('order_name');
        $order->phone = $request->input('phone');
        $order->branch_id = $request->input('branch_id');
        $order->product_name_id = $request->input('product_name_id');
        $order->product_price = $request->input('product_price');
        $order->order_quantity = $request->input('order_quantity');
        $order->description = $request->input('description');
        $order->responder_id = $request->input('responder_id');
        $order->update();
        return redirect()->back()->with('update', '');
    }

    public function destroy($id)
    {
        Order::where('id', $id)->delete();
        return redirect()->route('order')->with('delete', '');
    }
}
