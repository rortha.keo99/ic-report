<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Category;
use App\Models\Condition;
use App\Models\Status;
use App\Models\User;
class ServiceCheck extends Model
{
    use HasFactory;
    protected $table = 'service_checks';
    protected $fillable = [
        'equip_name',
        'cause',
        'asset_code',
        'asset_previous_code',
        'service_tage',
        'category_id',
        'branch_id',
        'inspection_check',
        'responded_by',
        'verified_by',
        'part_name',
        'part_service_tag',
        'old_serial',
        'new_serial',
        'purchase_date',
        'condition_id',
        'contact_person',
        'remark',
        'statuses_id',
        'file_upload',
    ];

    public function Category(){
        return $this->belongsTo(Category::class, 'category_id', 'id');
    }

    public function Branch(){
        return $this->belongsTo(Branch::class);
    }

    public function Status(){
        return $this->belongsTo(Status::class, 'statuses_id', 'id');
    }

    public function Condition(){
        return $this->belongsTo(Condition::class);
    }

    public function User(){
        return $this->belongsTo(User::class,'responded_by', 'id');
    }

    public function FileUpload(){
        return $this->belongsTo(FileUpload::class, 'file_upload_id', 'id');
    }

}
