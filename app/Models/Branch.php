<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\ServiceCheck;
class Branch extends Model
{
    use HasFactory;
    protected $table = 'branches';
    protected $fillable = [
        'branch_code',
        'branch_name',
        'description'
    ];

    public function ServiceCheck(){
        return $this->hasMany(ServiceCheck::class);
    }
}
