<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;
    protected $table = 'products';
    protected $fillable = [
        'barcode',
        'product_name',
        'product_type',
        'quantity',
        'unit_id',
        'product_price',
        'currency_id',
        'description',
        'product_status_id',
        'responder_id',
        'image'
    ];

    public function ProductType() {
        return $this->belongsTo(ProductType::class, 'product_type', 'id');
    }

    public function Unit() {
        return $this->belongsTo(Unit::class, 'unit_id', 'id');
    }

    public function Status() {
        return $this->belongsTo(ProductStatus::class, 'product_status_id', 'id');
    }

    public function User() {
        return $this->belongsTo(User::class, 'responder_id', 'id');
    }

    public function Currency() {
        return $this->belongsTo(Currency::class, 'currency_id', 'id');
    }
}
