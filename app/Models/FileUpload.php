<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\ServiceCheck;
use App\Models\Status;

class FileUpload extends Model
{
    use HasFactory;
    protected $table = 'fileupload';
    protected $fillable = [
        'image',
        'code_number_id',
        'status_id',
        'description'
    ];

    public function Service() {
        return $this->belongsTo(ServiceCheck::class, 'code_number_id');
    }

}
