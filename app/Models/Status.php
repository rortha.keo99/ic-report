<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Status extends Model
{
    use HasFactory;
    protected $table = 'statuses';
    protected $fillable = ([
        'status_name',
        'description'
    ]);

    public function ServiceCheck(){
        return $this->hasMany(ServiceCheck::class);
    }
}
