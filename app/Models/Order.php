<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use HasFactory;
    protected $table = 'orders';
    protected $fillable = [
        'code_order',
        'order_name',
        'phone',
        'branch_id',
        'product_name_id',
        'product_price',
        'order_quantity',
        'description',
        'responder_id',
    ];

    public function Branch() {
        return $this->belongsTo(Branch::class, 'branch_id', 'id');
    }

    public function Product() {
        return $this->belongsTo(Product::class, 'product_name_id', 'id');
    }

    public function Unit() {
        return $this->belongsTo(Unit::class, 'unit_id', 'id');
    }

    public function Responder() {
        return $this->belongsTo(User::class, 'responder_id', 'id');
    }
}
