//Data table
$(document).ready(function () {
    $('#dataTable').DataTable();
    searching: true;
    $('[type=search]').each(function () {
        $(this).attr("placeholder", "Search...");
        $(this).before('<span class="fa fa-search"></span>');
    });
});

// check all checkbox
document.getElementById('check-all').onclick = function () {
    var checkboxes = document.getElementsByName('checkbox');
    for (var checkbox of checkboxes) {
        checkbox.checked = this.checked;
    }
}

//delete sweet alert
$('.delete-confirm').on('click', function (event) {
    event.preventDefault();
    const url = $(this).attr('href');
    swal({
        title: 'Are you sure?',
        text: 'This record will permanantly delete!',
        icon: 'warning',
        dangerMode: true,
        buttons: ["Cancel", "Yes"],
    }).then(function (value) {
        if (value) {
            window.location.href = url;
        }
    });
});

//export word confirm alert
$('.download-confirm').on('click', function (event) {
    event.preventDefault();
    const url = $(this).attr('href');
    swal({
        title: 'Are you sure?',
        text: 'This file will download now!',
        icon: 'success',
        dangerMode: false,
        buttons: ["Cancel", "Yes"],
    }).then(function (value) {
        if (value) {
            window.location.href = url;
        }
    });
});

//button add row service
$(document).ready(function () {
    var html =
        '<div class="form-row"><div class="form-group col-md-4"><label for="inputState">Part Name:</label><input type="text" class="form-control" id="inputCity" name="part_name" placeholder="Battery..." autocomplete="off"></div><div class="form-group col-md-3"><label for="inputZip">Service Tag:</label><input type="text" class="form-control" id="inputZip" name="part_service_tag" placeholder="HJD34DD..." autocomplete="off"></div><div class="form-group col-md-2"><label for="inputZip">Old Serial:</label><input type="text" class="form-control" id="inputZip" name="old_serial" placeholder="SRDE2389" autocomplete="off"></div><div class="form-group col-md-2"><label for="inputZip">New Serial:</label><input type="text" class="form-control" id="inputZip" name="new_serial" placeholder="SRDE2322" autocomplete="off"></div><div class="form-group col-md-1"><label for="inputState">Remove</label><input type="button" class="btn btn-danger" name="remove" id="remove" value="-"></div></div>'
    var min = 1;
    var max = 5;
    $("#add").click(function () {
        if (min <= max) {
            $("#row_field").append(html);
            min++;
        }
    });

    $(document).on('click', '#remove', function () {
        $(this).closest('.form-row').remove();
    });
});

//Edit
$(document).ready(function () {

    //Service Save
    $(document).on('click', '.savebtnService', function (e) {
        e.preventDefault();
        var data = {
            'equip_name': $('.equip_name').val(),
            'cause': $('.cause').val(),
            'asset_code': $('.asset_code').val(),
            'asset_previous_code': $('.asset_previous_code').val(),
            'service_tage': $('.service_tage').val(),
            'category_id': $('.category_id').val(),
            'branch_id': $('.branch_id').val(),
            'purchase_date': $('.purchase_date').val(),
            'condition_id': $('.condition_id').val(),
            'contact_person': $('.contact_person').val(),
            'inspection_check': $('.inspection_check').val(),
            'part_name': $('.part_name').val(),
            'part_service_tag': $('.part_service_tag').val(),
            'old_serial': $('.old_serial').val(),
            'new_serial': $('.new_serial').val(),
            'remark': $('.remark').val(),
            'responded_by': $('.responded_by').val(),
            'verified_by': $('.verified_by').val(),
            'statuses_id': $('.statuses_id').val(),
        }
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: "POST",
            url: "/service/save",
            data: data,
            dataType: "json",
            success: function (response) {
                console.log(response);
                if (response.status == 400) {
                    $('#error_message').html("");
                    $('#error_message').addClass('alert error-message');
                    $.each(response.errors, function (key, err_values) {
                        $('#error_message').append('<li>' + err_values + '</li>');
                    });
                }
                else {
                    $('#error_message').html("");
                    $('#error_message').removeClass('alert error-message');
                    // $('#success_message').addClass('alert alert-success');
                    // $('#success_message').text(response.message);
                    // $('#ModalCreate').modal('hide');
                    $('#ModalCreate').find('input').val("");
                    // location.reload();
                    swal("Congratulation!", "Your data has been saved", "success");
                }
            }
        });
    });

    //Category Save
    $(document).on('click', '.savebtnCategory', function (e) {
        e.preventDefault();
        var data = {
            'category_name': $('.category_name').val(),
            'description': $('.description').val(),
        }
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: "POST",
            url: "/category/save",
            data: data,
            dataType: "json",
            success: function (response) {
                console.log(response);
                if (response.status == 400) {
                    $('#error_message').html("");
                    $('#error_message').addClass('alert error-message');
                    $.each(response.errors, function (key, err_values) {
                        $('#error_message').append('<li>' + err_values + '</li>');
                    });
                }
                else {
                    $('#error_message').html("");
                    $('#error_message').removeClass('alert error-message');
                    // $('#success_message').addClass('alert alert-success');
                    // $('#success_message').text(response.message);
                    // $('#ModalCreate').modal('hide');
                    $('#ModalCreate').find('input').val("");
                    // location.reload();
                    swal("Congratulation!", "Your data has been saved", "success");
                }
            }
        });
    });

    //Branch Save
    $(document).on('click', '.savebtnBranch', function (e) {
        e.preventDefault();
        var data = {
            'branch_code': $('.branch_code').val(),
            'branch_name': $('.branch_name').val(),
            'description': $('.description').val(),
        }
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: "POST",
            url: "/branch/save",
            data: data,
            dataType: "json",
            success: function (response) {
                console.log(response);
                if (response.status == 400) {
                    $('#error_message').html("");
                    $('#error_message').addClass('alert error-message');
                    $.each(response.errors, function (key, err_values) {
                        $('#error_message').append('<li>' + err_values + '</li>');
                    });
                }
                else {
                    $('#error_message').html("");
                    $('#error_message').removeClass('alert error-message');
                    // $('#success_message').addClass('alert alert-success');
                    // $('#success_message').text(response.message);
                    // $('#ModalCreate').modal('hide');
                    $('#ModalCreate').find('input').val("");
                    // location.reload();
                    swal("Congratulation!", "Your data has been saved", "success");
                }
            }
        });
    });

    //Status Save
    $(document).on('click', '.savebtnStatus', function (e) {
        e.preventDefault();
        var data = {
            'status_name': $('.status_name').val(),
            'description': $('.description').val(),
        }
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: "POST",
            url: "/status/save",
            data: data,
            dataType: "json",
            success: function (response) {
                console.log(response);
                if (response.status == 400) {
                    $('#error_message').html("");
                    $('#error_message').addClass('alert error-message');
                    $.each(response.errors, function (key, err_values) {
                        $('#error_message').append('<li>' + err_values + '</li>');
                    });
                }
                else {
                    $('#error_message').html("");
                    $('#error_message').removeClass('alert error-message');
                    // $('#success_message').addClass('alert alert-success');
                    // $('#success_message').text(response.message);
                    // $('#ModalCreate').modal('hide');
                    $('#ModalCreate').find('input').val("");
                    // location.reload();
                    swal("Congratulation!", "Your data has been saved", "success");
                }
            }
        });
    });

    //Condition Save
    $(document).on('click', '.savebtnCondition', function (e) {
        e.preventDefault();
        var data = {
            'condition_name': $('.condition_name').val(),
            'description': $('.description').val(),
        }
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: "POST",
            url: "/conditon/save",
            data: data,
            dataType: "json",
            success: function (response) {
                console.log(response);
                if (response.status == 400) {
                    $('#error_message').html("");
                    $('#error_message').addClass('alert error-message');
                    $.each(response.errors, function (key, err_values) {
                        $('#error_message').append('<li>' + err_values + '</li>');
                    });
                }
                else {
                    $('#error_message').html("");
                    $('#error_message').removeClass('alert error-message');
                    // $('#success_message').addClass('alert alert-success');
                    // $('#success_message').text(response.message);
                    // $('#ModalCreate').modal('hide');
                    $('#ModalCreate').find('input').val("");
                    // location.reload();
                    swal("Congratulation!", "Your data has been saved", "success");
                }
            }
        });
    });

    //Product Status Save
    $(document).on('click', '.savebtnProductStatus', function (e) {
        e.preventDefault();
        var data = {
            'pro_status': $('.pro_status').val(),
            'description': $('.description').val(),
        }
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: "POST",
            url: "/prostatus/save",
            data: data,
            dataType: "json",
            success: function (response) {
                console.log(response);
                if (response.status == 400) {
                    $('#error_message').html("");
                    $('#error_message').addClass('alert error-message');
                    $.each(response.errors, function (key, err_values) {
                        $('#error_message').append('<li>' + err_values + '</li>');
                    });
                }
                else {
                    $('#error_message').html("");
                    $('#error_message').removeClass('alert error-message');
                    // $('#success_message').addClass('alert alert-success');
                    // $('#success_message').text(response.message);
                    // $('#ModalCreate').modal('hide');
                    $('#ModalCreate').find('input').val("");
                    // location.reload();
                    swal("Congratulation!", "Your data has been saved", "success");
                }
            }
        });
    });

    //Unit Save
    $(document).on('click', '.savebtnUnit', function (e) {
        e.preventDefault();
        var data = {
            'unit_name': $('.unit_name').val(),
            'description': $('.description').val(),
        }
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: "POST",
            url: "/unit/save",
            data: data,
            dataType: "json",
            success: function (response) {
                console.log(response);
                if (response.status == 400) {
                    $('#error_message').html("");
                    $('#error_message').addClass('alert error-message');
                    $.each(response.errors, function (key, err_values) {
                        $('#error_message').append('<li>' + err_values + '</li>');
                    });
                }
                else {
                    $('#error_message').html("");
                    $('#error_message').removeClass('alert error-message');
                    // $('#success_message').addClass('alert alert-success');
                    // $('#success_message').text(response.message);
                    // $('#ModalCreate').modal('hide');
                    $('#ModalCreate').find('input').val("");
                    // location.reload();
                    swal("Congratulation!", "Your data has been saved", "success");
                }
            }
        });
    });

    //Order Save
    $(document).on('click', '.savebtnOrder', function (e) {
        e.preventDefault();
        var data = {
            'order_name': $('.order_name').val(),
            'phone': $('.phone').val(),
            'branch_id': $('.branch_id').val(),
            'product_name_id': $('.product_name_id').val(),
            'order_quantity': $('.order_quantity').val(),
            'description': $('.description').val(),
            'responder_id': $('.responder_id').val(),
        }
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: "POST",
            url: "/order/save",
            data: data,
            dataType: "json",
            success: function (response) {
                console.log(response);
                if (response.status == 400) {
                    $('#error_message').html("");
                    $('#error_message').addClass('alert error-message');
                    $.each(response.errors, function (key, err_values) {
                        $('#error_message').append('<li>' + err_values + '</li>');
                    });
                }
                else {
                    $('#error_message').html("");
                    $('#error_message').removeClass('alert error-message');
                    // $('#success_message').addClass('alert alert-success');
                    // $('#success_message').text(response.message);
                    // $('#ModalCreate').modal('hide');
                    $('#ModalCreate').find('input').val("");
                    // location.reload();
                    swal("Congratulation!", "Your data has been saved", "success");
                }
            }
        });
    });

    //Category edit
    $(document).on('click', '.editbtnCategory', function (e) {
        e.preventDefault();
        var cate_id = $(this).val();
        $('#ModalEdit').modal('show');
        $.ajax({
            type: "GET",
            url: "/category/edit/" + cate_id,
            success: function (response) {
                console.log(response);
                $('#category_name').val(response.category.category_name);
                $('#description').val(response.category.description);
                $('#cate_id').val(cate_id);
            }
        });
    });

    //Branch edit
    $(document).on('click', '.editbtnBranch', function (e) {
        e.preventDefault();
        var branch_id = $(this).val();
        $('#ModalEdit').modal('show');
        $.ajax({
            type: "GET",
            url: "/branch/edit/" + branch_id,
            success: function (response) {
                console.log(response);
                $('#branch_code').val(response.branch.branch_code);
                $('#branch_name').val(response.branch.branch_name);
                $('#description').val(response.branch.description);
                $('#branch_id').val(branch_id);
            }
        });
    });

    //Status edit
    $(document).on('click', '.editbtnStatus', function (e) {
        e.preventDefault();
        var status_id = $(this).val();
        $('#ModalEdit').modal('show');
        $.ajax({
            type: "GET",
            url: "/status/edit/" + status_id,
            success: function (response) {
                $('#status_name').val(response.status.status_name);
                $('#description').val(response.status.description);
                $('#status_id').val(status_id);
            }
        });
    });

    //Service edit
    $(document).on('click', '.editbtnService', function (e) {
        e.preventDefault();
        var service_id = $(this).val();
        $('#ModalEdit').modal('show');
        process: true,
            $.ajax({
                type: "GET",
                url: "/service/edit/" + service_id,
                success: function (response) {
                    console.log(response);
                    $('#code_number').val(response.service.code_number);
                    $('#equip_name').val(response.service.equip_name);
                    $('#cause').val(response.service.cause);
                    $('#asset_code').val(response.service.asset_code);
                    $('#asset_previous_code').val(response.service.asset_previous_code);
                    $('#service_tage').val(response.service.service_tage);
                    $('#category_id').val(response.service.category_id);
                    $('#branch_id').val(response.service.branch_id);
                    $('#purchase_date').val(response.service.purchase_date);
                    $('#condition_id').val(response.service.condition_id);
                    $('#contact_person').val(response.service.contact_person);
                    $('#inspection_check').val(response.service.inspection_check);
                    $('#part_name').val(response.service.part_name);
                    $('#part_service_tag').val(response.service.part_service_tag);
                    $('#old_serial').val(response.service.old_serial);
                    $('#new_serial').val(response.service.new_serial);
                    $('#remark').val(response.service.remark);
                    $('#responded_by').val(response.service.responded_by);
                    $('#verified_by').val(response.service.verified_by);
                    $('#statuses_id').val(response.service.statuses_id);
                    $('#service_id').val(service_id);
                }
            });
    });

    //User edit
    $(document).on('click', '.editbtnUser', function (e) {
        e.preventDefault();
        var user_id = $(this).val();
        $('#ModalEdit').modal('show');
        $.ajax({
            type: "GET",
            url: "/user/edit/" + user_id,
            success: function (response) {
                console.log(response);
                $('#u_name').val(response.user.name);
                $('#u_email').val(response.user.email);
                $('#u_phone').val(response.user.phone);
                $('#user_id').val(user_id);
            }
        });
    });

    //View Service
    $(document).on('click', '.viewbtnService', function (e) {
        e.preventDefault();
        var service_id = $(this).val();
        $('#ModalShow').modal('show');
        $.ajax({
            type: "GET",
            url: "/service/show/" + service_id,
            success: function (response) {
                console.log(response);
                $('#sv_service_id').html(service_id);
                $('#sv_code_number').html(response.service.code_number);
                $('#sv_equip_name').html(response.service.equip_name);
                $('#sv_cause').html(response.service.cause);
                $('#sv_asset_code').html(response.service.asset_code);
                $('#sv_asset_previous_code').html(response.service.asset_previous_code);
                $('#sv_service_tage').html(response.service.service_tage);
                $('#sv_category_id').html(response.service.category_id);
                $('#sv_branch_id').html(response.service.branch_id);
                $('#sv_purchase_date').html(response.service.purchase_date);
                $('#sv_condition_id').html(response.service.condition_id);
                $('#sv_contact_person').html(response.service.contact_person);
                $('#sv1_contact_person').html(response.service.contact_person);
                $('#sv_inspection_check').html(response.service.inspection_check);
                $('#sv_part_name').html(response.service.part_name);
                $('#sv_part_service_tag').html(response.service.part_service_tag);
                $('#sv_old_serial').html(response.service.old_serial);
                $('#sv_new_serial').html(response.service.new_serial);
                $('#sv_remark').html(response.service.remark);
                $('#sv_responded_by').html(response.service.responded_by);
                $('#sv_verified_by').html(response.service.verified_by);
                $('#sv_statuses_id').html(response.service.statuses_id);
                $('#sv_created_at').html(response.service.created_at);
                $('#sv1_created_at').html(response.service.created_at);
                $('#sv2_created_at').html(response.service.created_at);
                $('#sv3_created_at').html(response.service.created_at);
            }
        });
    });

    //Unit edit
    $(document).on('click', '.editbtnUnit', function (e) {
        e.preventDefault();
        $unit_id = $(this).val();
        $('#ModalEdit').modal('show');
        $.ajax({
            type: "GET",
            url: "/unit/edit/" + $unit_id,
            success: function (response) {
                console.log(response);
                $('#unit_name').val(response.unit.unit_name);
                $('#description').val(response.unit.description);
                $('#unit_id').val($unit_id);
            }
        });
    });

    //Product type edit
    $(document).on('click', '.editbtnProducttype', function () {
        $protype_id = $(this).val();
        $('#ModalEdit').modal('show');
        $.ajax({
            type: "GET",
            url: "/protype/edit/" + $protype_id,
            success: function (response) {
                console.log(response);
                $('#typename').val(response.protype.type_name);
                $('#description').val(response.protype.description);
                // $('#image').val(response.protype.image);
                $('#protype_id').val($protype_id);
            }
        });
    });

    //Product status edit
    $(document).on('click', '.editbtnProStatus', function () {
        $prostatus_id = $(this).val();
        $('#ModalEdit').modal('show');
        $.ajax({
            type: "GET",
            url: "/prostatus/edit/" + $prostatus_id,
            success: function (response) {
                console.log(response);
                $('#pro_status').val(response.prostatus.pro_status);
                $('#description').val(response.prostatus.description);
                $('#prostatus_id').val($prostatus_id);
            }
        });

    });

    //Condition edit
    $(document).on('click', '.editbtnCondition', function () {
        $condition_id = $(this).val();
        $('#ModalEdit').modal('show');
        $.ajax({
            type: "GET",
            url: "/condition/edit/" + $condition_id,
            success: function (response) {
                console.log(response);
                $('#condition_name').val(response.condition.condition_name);
                $('#description').val(response.condition.description);
                $('#condition_id').val($condition_id);
            }
        });
    });

    //Product edit
    $(document).on('click', '.editbtnProduct', function () {
        $product_id = $(this).val();
        $('#ModalEdit').modal('show');
        $.ajax({
            type: "GET",
            url: "/product/edit/" + $product_id,
            success: function (response) {
                console.log(response);
                $('#barcode').val(response.product.barcode);
                $('#product_name').val(response.product.product_name);
                $('#product_type').val(response.product.product_type);
                $('#quantity').val(response.product.quantity);
                $('#unit_id').val(response.product.unit_id);
                $('#product_price').val(response.product.product_price);
                $('#currency_id').val(response.product.currency_id);
                $('#product_status_id').val(response.product.product_status_id);
                $('#responder_id').val(response.product.responder_id);
                $('#description').val(response.product.description);
                $('#product_id').val($product_id);
            }
        });
    });

    //Order edit
    $(document).on('click', '.editbtnOrder', function () {
        $order_id = $(this).val();
        $('#ModalEdit').modal('show');
        $.ajax({
            type: "GET",
            url: "/order/edit/" + $order_id,
            success: function (response) {
                $('#order_name').val(response.order.order_name);
                $('#phone').val(response.order.phone);
                $('#branch_id').val(response.order.branch_id);
                $('#product_name_id').val(response.order.product_name_id);
                $('#product_price').val(response.order.product_price);
                $('#order_quantity').val(response.order.order_quantity);
                $('#responder_id').val(response.order.responder_id);
                $('#description').val(response.order.description);
                $('#order_id').val($order_id);
            }
        });
    });

    //Report edit
    $(document).on('click', '.editbtnReport', function () {
        $report_id = $(this).val();
        $('#ModalEdit').modal('show');
        $.ajax({
            type: "GET",
            url: "/report/edit/" + $report_id,
            success: function (response) {
                console.log(response);
                $('#kh_name').val(response.report.kh_name);
                $('#eng_name').val(response.report.eng_name);
                $('#description').val(response.report.description);
                $('#report_id').val($report_id);
            }
        });
    });
});
