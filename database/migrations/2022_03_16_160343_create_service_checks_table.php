<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    public function up()
    {
        Schema::create('service_checks', function (Blueprint $table) {
            $table->id();
            $table->string('code_number')->nullable();
            $table->string('equip_name');
            $table->string('cause')->nullable();
            $table->string('asset_code')->nullable();
            $table->string('asset_previous_code')->nullable();
            $table->string('service_tage')->nullable();
            $table->integer('category_id');
            $table->integer('branch_id');
            $table->date('purchase_date')->nullable();
            $table->integer('condition_id')->nullable();
            $table->string('contact_person')->nullable();
            $table->text('inspection_check');
            $table->string('responded_by');
            $table->string('verified_by')->nullable();
            $table->string('part_name')->nullable();
            $table->string('part_service_tag')->nullable();
            $table->string('old_serial')->nullable();
            $table->string('new_serial')->nullable();
            $table->string('remark')->nullable();
            $table->integer('statuses_id');
            $table->integer('file_upload_id')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('service_checks');
    }
};
