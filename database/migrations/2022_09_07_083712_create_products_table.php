<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->string('code_item');
            $table->string('barcode')->nullable();
            $table->string('product_name');
            $table->integer('product_type');
            $table->integer('quantity');
            $table->integer('unit_id');
            $table->double('product_price')->nullable();
            $table->integer('currency_id')->nullable();
            $table->string('description')->nullable();
            $table->integer('product_status_id')->nullable();
            $table->integer('responder_id');
            $table->string('image')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
};
