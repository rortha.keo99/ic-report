<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->string('code_order')->nullable();
            $table->string('order_name');
            $table->string('phone')->nullable();
            $table->integer('branch_id')->nullable();
            $table->integer('product_name_id');
            $table->double('product_price')->nullable();
            $table->integer('order_quantity');
            $table->string('description')->nullable();
            $table->integer('responder_id')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
};
