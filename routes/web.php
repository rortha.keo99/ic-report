<?php

use App\Http\Controllers\Admin\DashboardController;
use App\Http\Controllers\BranchController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\ConditionController;
use App\Http\Controllers\FileUploadController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\OrderController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\ProductTypeController;
use App\Http\Controllers\ServiceCheckController;
use App\Http\Controllers\StatusController;
use App\Http\Controllers\UnitController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\ProductStatusController;
use App\Http\Controllers\ReportController;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

Route::get('/', function () {
    return view('auth.login');
});

Route::middleware('auth')->group(function () {

    // Category
    Route::controller(CategoryController::class)->group(function () {
        Route::get('/category', 'index')->name('category');
        Route::post('/category/save', 'store');
        Route::get('/category/edit/{id}', 'edit');
        Route::put('/category/update', 'update');
        Route::get('/category/delete/{id}', 'destroy');
    });

    //Branch
    Route::controller(BranchController::class)->group(function () {
        Route::get('/branch', 'index')->name('branch');
        Route::post('/branch/save', 'store');
        Route::get('/branch/delete/{id}', 'destroy');
        Route::get('/branch/edit/{id}', 'edit');
        Route::put('/branch/update', 'update');
    });
    
    //Status
    Route::controller(StatusController::class)->group(function () {
        Route::get('/status', 'index')->name('status');
        Route::post('/status/save', 'store');
        Route::get('/status/delete/{id}', 'destroy');
        Route::get('/status/edit/{id}', 'edit');
        Route::put('/status/update', 'update');
    });
    
    //Service
    Route::controller(ServiceCheckController::class)->group(function () {
        Route::get('/service', 'index')->name('service');
        Route::post('/service/save', 'store');
        Route::get('/service/delete/{id}', 'destroy');
        Route::get('/service/export/{id}', 'wordexport');
        Route::get('/service/edit/{id}', 'edit');
        Route::put('/service/update', 'update');
        Route::get('/service/show/{id}', 'show');
    });
    Route::controller(FileUploadController::class)->group(function () {
        Route::post('/service/storeupload', 'store');
    });

    //Condition
    Route::controller(ConditionController::class)->group(function () {
        Route::get('/condition', 'index')->name('condition');
        Route::post('/conditon/save', 'store');
        Route::get('/condition/edit/{id}', 'edit');
        Route::get('/condition/delete/{id}', 'destroy');
        Route::put('/conditon/update', 'update');
    });

    //Users
    Route::controller(UserController::class)->group(function () {
        Route::get('/user', 'index')->name('user');
        Route::post('/user/save', 'store');
        Route::get('/user/edit/{id}', 'edit');
        Route::put('/user/update', 'update');
    });
    
    //Product
    Route::controller(ProductController::class)->group(function () {
        Route::get('/product', 'index')->name('product');
        Route::post('/product/save', 'store');
        Route::get('/product/edit/{id}', 'edit');
        Route::get('/product/delete/{id}', 'destroy');
        Route::put('/product/update', 'update');
    });

    //Product type
    Route::controller(ProductTypeController::class)->group(function () {
        Route::get('/protype', 'index')->name('protype');
        Route::post('/protype/save', 'store');
        Route::get('/protype/edit/{id}', 'edit');
        Route::get('/protype/delete/{id}', 'destroy');
        Route::put('/protype/update', 'update');
    });

    //Product status
    Route::controller(ProductStatusController::class)->group(function () {
        Route::get('/prostatus', 'index')->name('prostatus');
        Route::post('/prostatus/save', 'store');
        Route::get('/prostatus/edit/{id}', 'edit');
        Route::get('/prostatus/delete/{id}', 'destroy');
        Route::put('/prostatus/update', 'update');
    });

    //Order
    Route::controller(OrderController::class)->group(function () {
        Route::get('/order', 'index')->name('order');
        Route::post('/order/save', 'store');
        Route::get('/order/edit/{id}', 'edit');
        Route::get('/order/delete/{id}', 'destroy');
        Route::put('/order/update', 'update');
    });

    //Unit
    Route::controller(UnitController::class)->group(function () {
        Route::get('/unit', 'index')->name('unit');
        Route::post('/unit/save', 'store');
        Route::get('/unit/edit/{id}', 'edit');
        Route::get('/unit/delete/{id}', 'destroy');
        Route::put('/unit/update', 'update');
    });

    Route::controller(DashboardController::class)->group(function () {
        Route::get('/dashboard', 'index')->name('dashboard');
    });

    Route::controller(ReportController::class)->group(function () {
        Route::get('/report', 'index')->name('report');
        Route::post('/report/save', 'store');
        Route::get('/report/edit/{id}', 'edit');
        Route::put('/report/update', 'update');
        Route::get('/report/delete/{id}', 'destroy');
    });

    Route::controller(HomeController::class)->group(function () {
        Route::get('/', 'index')->name('home');
    });
});

Auth::routes();


    


